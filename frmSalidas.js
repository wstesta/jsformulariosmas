// Referencias a elementos del formulario
var contact;
var interaction;
var contactInputsById;
var notificationsService;

// Necesario refresco contactos via WS
var component = this;
var firsttime = true;
var actualizar = false;
var wsUrl = "https://5d605.inconcertcc.com:10443/";

// Datos custom del contacto
var contactCustomData = {};

// Evento de formulario inicializado
this.onReady = function() {

    // Obtener y guardar referencias a elementos del formulario
    contact = this.contact;
    interaction = this.interaction;
    contactInputsById = this.contactInputsById;
    notificationsService = this.notificationsService;

    // Si hay interacción programar un timer de 1 minuto
    if (interaction) setTimeout(function() {
        // Mostrar notificación de nivel info al usuario
        notificationsService.info("Información", "La interacción lleva 1 minuto.");
    }, 60000);

    // Si hay interacción programar un timer de 5 minutos
    if (interaction) setTimeout(function() {
        // Mostrar notificación de nivel info al usuario
        notificationsService.info("Información", "La interacción lleva 5 minutos.");
    }, 300000);
};

// Evento de contacto cargado
this.onLoaded = function(_contact) {

    // Hay que actualizar la referencia al objeto contacto
    contact = _contact;

// Llamada al WS para actualizar el contacto
    var requestOptions = {
    method: 'GET',
    redirect: 'follow',
    mode: 'cors'
    };

  fetch(wsUrl + "/api/CRM/GetInquilino?id='" + contact.id + "'", requestOptions)
    .then(function (response) {
      response.json()
      .then(function (OK) {
        var contact_crm = OK[0];

        try {
          contactCustomData = JSON.parse(contact.customData);
        } catch (err) {
          console.error("Ocurrio un error al obtener datos personalizados del contacto.");
          console.error(err);
        }

        if (contact_crm != undefined) {
          contact = mergeContacts(contact, contact_crm, contactCustomData);
          actualizar = true;
        }

        if (firsttime) {
          component.InitializeContact();
          firsttime = false;
        }
        
        try {
            contactCustomData = JSON.parse(contact.customData);
          } catch (err) {
            console.error("Ocurrio un error al obtener datos personalizados del contacto.");
            console.error(err);
          }
        // Escribir a log
        if (contact) console.log("Se cargaron datos del contacto '" + contact.id + "'");
       
      })
      .catch(function (KO) {
        console.log('Request failed: ', KO);
        notificationsService.warn("Atención", "Error al cargar los datos de Dynamics: " + KO);
      })
    }).catch(function (error) {
      console.log('Request failed: ', error)
      notificationsService.warn("Atención", "Error al cargar los datos de Dynamics: " + error);
    });
}

// Evento de cambio en propiedad de contacto
this.onContactPropertyChanged = function(property, value, _contact) {
    
    // Escribir a log
    console.log("Información", "La propiedad '" + property + "' cambió a valor '" + value + "'");
   
};

// Evento de antes de guardar
this.onBeforeSave = function(_contact, mustFinishInteraction) {
    // Para actualizar el contacto en su CRM
    actualizar = true;
    // Flag de impedir guardado
    var preventSave = false;

    // Si el formulario muestra el firstname y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.firstname && _contact && _contact.firstname && !validateAlphanumeric(_contact.firstname)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El nombre ingresado para el contacto (" + _contact.firstname + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el lastname y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.lastname && _contact && _contact.lastname && !validateAlphanumeric(_contact.lastname)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El apellido ingresado para el contacto (" + _contact.lastname + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el email y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.email && _contact && _contact.email && !validateEmail(_contact.email)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El correo ingresado para el contacto (" + _contact.email + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el phone y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.phone && _contact && _contact.phone && !validatePhone(_contact.phone)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El teléfono ingresado para el contacto (" + _contact.phone + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }
    // Para actualizar el contacto en su CRM
    if (actualizar){
        actualizarContacto(wsUrl, contact, contactCustomData);
    }

    // Permitir el guardado
    return !preventSave;
}

// Evento de despues de guardar
this.onAfterSave = function(_contact) {

    // Escribir a log
    if (_contact) console.log("Se guardaron datos del contacto '" + _contact.id + "'");
}

// Función de validación de campos de correo
function validateEmail(value) {

    // Expresión regular para validar campos de correo
    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!EMAIL_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

// Función de validación de campos de teléfono
function validatePhone(value) {

    // Expresión regular para validar campos de teléfono
    const PHONE_REGEXP = /^[0-9\-\+\(\)\s]+$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!PHONE_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

// Función de validación de campos alfanuméricos
function validateAlphanumeric(value) {

    // Expresión regular para validar campos alfanuméricos
    const ALPHA_REGEXP = /^[a-zA-Z\-\_ñÑàèìòùáéíóúäëïöüÀÈÌÒÙÁÉÍÓÚÄËÏÖÜ\.0-9\-\+\(\)\s]+$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!ALPHA_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}
///////////////////////////////////////
// Funciones para el WS //
///////////////////////////////////////

function actualizarContacto(_wsUrl, _contact) {
    console.log("actualizar contacto: '" + _contact.id + "'");
    var res = "KO";
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    
    var raw = JSON.stringify({
        "accountid": _contact.externalid,
        "eve_nombreapellidos": _contact.eve_nombreapellidos,
        "eve_nombre": _contact.firstname,
        "eve_apellidos": _contact.lastname,
        "eve_nif_nie": _contact.eve_nif_nie,
        "eve_telefono1": _contact.phone,
        // "eve_telefono2": _contact.Telefono2,
        "eve_email": _contact.email,
        "eve_observacion_gestion":_contact.eve_observacion_gestion
      });
    
    console.log(raw);

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
      mode: 'cors'
    };
    
    fetch(_wsUrl + "/api/CRM/inquilino_update", requestOptions)
    .then(function(response) {
        return response.text();
      })
      .then(function(text) {
        console.log('Request ', text);
        if(text != "OK"){
          notificationsService.warn("Atención", "Error al enviar los datos a Dynamics" + text);
        }
        res = text;
      })
      .catch(function(error) {
        console.log('Request failed ', error)
        notificationsService.warn("Atención", "Error al enviar los datos a Dynamics");
      });
    return res;
}

// Para que funcione el WS refrescar el contacto
function mergeContacts(contact, contact_crm, contactCustomData) {
    console.log("mergeContacts");
   //console.log("Contact: ", JSON.stringify(contact));
    //console.log("contact_crm: ", JSON.stringify(contact_crm));
    var customData = {};
    try {
      // Parseo de datos personalizados del contacto
      customData = JSON.parse(contact.customData);
    } catch (err) {
      // Ocurrio un error al hacer el parse
      console.error("Ocurrio un error al obtener datos personalizados del contacto.");
      console.error(err);
    }
    if (contact_crm != undefined && contact_crm != {} && contact_crm.name != undefined && contact_crm.name == contact.id) {
      contact.email = contact_crm.eve_email || contact.email;
      contact.firstname = contact_crm.eve_nombre || contact.firstname;
      contact.lastname = contact_crm.eve_apellidos || contact.lastname;
      contact.phone = contact_crm.eve_telefono1 || contact.phone;
      contact.mobile = contact_crm.eve_telefono2 || contact.mobile;
      contact.fax = contact_crm.eve_telefono3 || contact.fax;
      contact.address1 = contact_crm.eve_tipovia || contact.address1;
      contact.address2 = contact_crm.eve_calle || contact.address2;
      contact.city = contact_crm.eve_poblacion || contact.city;
  
  
      customData.eve_portal = contact_crm.eve_portal || contactCustomData.eve_portal;
      customData.eve_calificativo = contact_crm.eve_calificativo || contactCustomData.eve_calificativo;
      customData.eve_descripcioncalificativo = contact_crm.eve_descripcioncalificativo || contactCustomData.eve_descripcioncalificativo;
      customData.eve_estado_anterior_deuda = contact_crm.eve_estado_anterior_deuda || contactCustomData.eve_estado_anterior_deuda;
      customData.eve_observacion_carta = contact_crm.eve_observacion_carta || contactCustomData.eve_observacion_carta;
      customData.eve_nif_nie = contact_crm.eve_nif_nie || contactCustomData.eve_nif_nie;
      customData.eve_numero = contact_crm.eve_numero || contactCustomData.eve_numero;
      customData.eve_escalera = contact_crm.eve_escalera || contactCustomData.eve_escalera;
      customData.eve_piso = contact_crm.eve_piso || contactCustomData.eve_piso;
      customData.eve_puerta = contact_crm.eve_puerta || contactCustomData.eve_puerta;
      customData.eve_codigoPostal = contact_crm.eve_codigoPostal || contactCustomData.eve_codigoPostal;
      customData.eve_consentimiento_grupo = contact_crm.eve_consentimiento_grupo || contactCustomData.eve_consentimiento_grupo;
      customData.eve_consentimiento_terceras = contact_crm.eve_consentimiento_terceras || contactCustomData.eve_consentimiento_terceras;
      customData.eve_origen_consentimiento = contact_crm.eve_origen_consentimiento || contactCustomData.eve_origen_consentimiento;
      customData.eve_fecha_consentimiento = contact_crm.eve_fecha_consentimiento || contactCustomData.eve_fecha_consentimiento;
      customData.eve_descripcionsociedad = contact_crm.eve_descripcionsociedad || contactCustomData.eve_descripcionsociedad;
      customData.eve_nombreapellidos = contact_crm.eve_nombreapellidos || contactCustomData.eve_nombreapellidos;
      customData.eve_envio_carta_deuda = contact_crm.eve_envio_carta_deuda || contactCustomData.eve_envio_carta_deuda;
      customData.eve_estado_deuda = contact_crm.eve_estado_deuda || contactCustomData.eve_estado_deuda;
      customData.eve_fecha_cita = contact_crm.eve_fecha_cita || contactCustomData.eve_fecha_cita;
      customData.eve_fecha_cita_adicional = contact_crm.eve_fecha_cita_adicional || contactCustomData.eve_fecha_cita_adicional;
      customData.eve_fecha_promesa_pago = contact_crm.eve_fecha_promesa_pago || contactCustomData.eve_fecha_promesa_pago;
      customData.eve_fecha_ultima_cita = contact_crm.eve_fecha_ultima_cita || contactCustomData.eve_fecha_ultima_cita;
      customData.eve_fecha_ultima_gestion = contact_crm.eve_fecha_ultima_gestion || contactCustomData.eve_fecha_ultima_gestion;
      customData.eve_multiplessociedades = contact_crm.eve_multiplessociedades || contactCustomData.eve_multiplessociedades;
      customData.eve_gestion_de_impagos = contact_crm.eve_gestion_de_impagos || contactCustomData.eve_gestion_de_impagos;
      customData.eve_gestion_de_impagos_adicional = contact_crm.eve_gestion_de_impagos_adicional || contactCustomData.eve_gestion_de_impagos_adicional;
      customData.eve_gestora_deuda = contact_crm.eve_gestora_deuda || contactCustomData.eve_gestora_deuda;
      customData.eve_deudatotalinquilino = contact_crm.eve_deudatotalinquilino || contactCustomData.eve_deudatotalinquilino;
      customData.eve_deudatotalcontrato = contact_crm.eve_deudatotalcontrato || contactCustomData.eve_deudatotalcontrato;
      customData.eve_observacion_gestion = contact_crm.eve_observacion_gestion || contactCustomData.eve_observacion_gestion;
      customData.eve_observaciones_deuda = contact_crm.eve_observaciones_deuda || contactCustomData.eve_observaciones_deuda;
      customData.eve_deudatotalplandepago = contact_crm.eve_deudatotalplandepago || contactCustomData.eve_deudatotalplandepago;
      customData.eve_deudatotalanticipo = contact_crm.eve_deudatotalanticipo || contactCustomData.eve_deudatotalanticipo;
      customData.eve_importe_total_pendiente_vencimiento_plan = contact_crm.eve_importe_total_pendiente_vencimiento_plan || contactCustomData.eve_importe_total_pendiente_vencimiento_plan;
  
      contact.customData = JSON.stringify(customData);
  
      contact.eve_nif_nie = contact_crm.eve_nif_nie || contact.eve_nif_nie;
      contact.eve_numero = contact_crm.eve_numero || contact.eve_numero;
      contact.eve_escalera = contact_crm.eve_escalera || contact.eve_escalera;
      contact.eve_piso = contact_crm.eve_piso || contact.eve_piso;
      contact.eve_puerta = contact_crm.eve_puerta || contact.eve_puerta;
      contact.eve_codigoPostal = contact_crm.eve_codigoPostal || contact.eve_codigoPostal;
      contact.eve_fecha_consentimiento = contact_crm.eve_fecha_consentimiento || contact.eve_fecha_consentimiento;
      contact.eve_descripcionsociedad = contact_crm.eve_descripcionsociedad || contact.eve_descripcionsociedad;
      contact.name = contact_crm.name || contact.name;
      contact.eve_nombreapellidos = contact_crm.eve_nombreapellidos || contact.eve_nombreapellidos;
      contact.eve_fecha_cita = contact_crm.eve_fecha_cita || contact.eve_fecha_cita;
      contact.eve_fecha_cita_adicional = contact_crm.eve_fecha_cita_adicional || contact.eve_fecha_cita_adicional;
      contact.eve_fecha_ultima_gestion = contact_crm.eve_fecha_ultima_gestion || contact.eve_fecha_ultima_gestion;
      contact.eve_gestion_de_impagos = contact_crm.eve_gestion_de_impagos || contact.eve_gestion_de_impagos;
      contact.eve_gestion_de_impagos_adicional = contact_crm.eve_gestion_de_impagos_adicional || contact.eve_gestion_de_impagos_adicional;
      contact.eve_gestora_deuda = contact_crm.eve_gestora_deuda || contact.eve_gestora_deuda;
      contact.eve_observacion_gestion = contact_crm.eve_observacion_gestion || contact.eve_observacion_gestion;
      contact.eve_observaciones_deuda = contact_crm.eve_observaciones_deuda || contact.eve_observaciones_deuda;
      contact.eve_fecha_promesa_pago = contact_crm.eve_fecha_promesa_pago || contact.eve_fecha_promesa_pago;
      contact.eve_deudatotalinquilino = contact_crm.eve_deudatotalinquilino || contact.eve_deudatotalinquilino;
      contact.eve_deudatotalcontrato = contact_crm.eve_deudatotalcontrato || contact.eve_deudatotalcontrato;
      contact.eve_deudatotalplandepago = contact_crm.eve_deudatotalplandepago || contact.eve_deudatotalplandepago;
      contact.eve_deudatotalanticipo = contact_crm.eve_deudatotalanticipo || contact.eve_deudatotalanticipo;
  
    }
    //console.log("resultado: ", JSON.stringify(contact));
    return contact;
  }