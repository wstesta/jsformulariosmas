// Referencias a elementos del formulario
var contact;
var interaction;
var contactInputsById;
var notificationsService;

// Datos custom del contacto
var contactCustomData = {};
//Necesarios para el WS
var actualizar = false;
var actualizar_eve_fecha_cita = false;
var actualizar_eve_fecha_promesa_pago = false;
var wsUrl = "https://5d605.inconcertcc.com:10443/";
// Evento de formulario inicializado
this.onReady = function() {

    // Obtener y guardar referencias a elementos del formulario
    contact = this.contact;
    interaction = this.interaction;
    contactInputsById = this.contactInputsById;
    notificationsService = this.notificationsService;

    // Si hay interacción programar un timer de 1 minuto
    if (interaction) setTimeout(function() {
        // Mostrar notificación de nivel info al usuario
        notificationsService.info("Información", "La interacción lleva 1 minuto.");
    }, 60000);

    // Si hay interacción programar un timer de 5 minutos
    if (interaction) setTimeout(function() {
        // Mostrar notificación de nivel info al usuario
        notificationsService.info("Información", "La interacción lleva 5 minutos.");
    }, 300000);
};

// Evento de contacto cargado
this.onLoaded = function(_contact) {

    // Hay que actualizar la referencia al objeto contacto
    contact = _contact;

    console.log(JSON.stringify(contact));

    try {
        // Parseo de datos personalizados del contacto
        contactCustomData = JSON.parse(contact.customData);    
    } catch (err) {
        // Ocurrio un error al hacer el parse
        console.error("Ocurrio un error al obtener datos personalizados del contacto.");
        console.error(err);
    }

    // Escribir a log
    if (_contact) console.log("Se cargaron datos del contacto '" + _contact.id + "'");
    
    //Ocultar campos
   $("#customProperties_PreguntaM11").hide() 
   $("#customProperties_PreguntaM111").hide()
}

// Evento de cambio en propiedad de contacto
this.onContactPropertyChanged = function(property, value, _contact) {
  // Escribir a log
  console.log("Información", "La propiedad '" + property + "' cambió a valor '" + value + "'");
  
  // Para WS actualizar el contacto en su CRM
  if(property == 'eve_fecha_cita' || property == 'eve_fecha_promesa_pago'){
    if(value == undefined){
      notificationsService.warn("Atención", "La fecha no puede estar vacia, debe ser AAAA-MM-DD");
      actualizar = false;
    }
    else{
      if(validateDate(value)){
        // Para WS actualizar el contacto en su CRM
        actualizar = true;
        notificationsService.success("Correcto", "El formato de la fecha es correcto");
      }
      else{
        notificationsService.warn("Atención", "El formato de fecha correcto es AAAA-MM-DD");
        actualizar = false;
      }
      
    }
  }
  else{
    // Para WS actualizar el contacto en su CRM
    actualizar = true;
  }
  // Para WS actualizar el contacto en su CRM
  console.log("La información del contacto se actualizará al guardar: " + actualizar);    
  
  if(property == 'PreguntaM1') {
      if(value == 'No voy a pagar'){
              $("#customProperties_PreguntaM11").show()
      }else{
              $("#customProperties_PreguntaM11").hide()  
      }
  }
  
  if(property == 'PreguntaM11') {
      if(value == 'Otros motivos'){
              $("#customProperties_PreguntaM111").show()
      }else{
              $("#customProperties_PreguntaM111").hide()  
      }
  }
};

// Evento de antes de guardar
this.onBeforeSave = function(_contact, mustFinishInteraction) {

    // Flag de impedir guardado
    var preventSave = false;

    // Si el formulario muestra el firstname y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.firstname && _contact && _contact.firstname && !validateAlphanumeric(_contact.firstname)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El nombre ingresado para el contacto (" + _contact.firstname + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el lastname y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.lastname && _contact && _contact.lastname && !validateAlphanumeric(_contact.lastname)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El apellido ingresado para el contacto (" + _contact.lastname + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el email y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.email && _contact && _contact.email && !validateEmail(_contact.email)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El correo ingresado para el contacto (" + _contact.email + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el phone y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.phone && _contact && _contact.phone && !validatePhone(_contact.phone)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El teléfono ingresado para el contacto (" + _contact.phone + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // control de las fechas
    // if()

    // Para WS actualizar el contacto en su CRM
    if (actualizar){
        actualizarContacto(wsUrl, _contact);
    }
    // Permitir el guardado
    return !preventSave;
}

// Evento de despues de guardar
this.onAfterSave = function(_contact) {

    // Escribir a log
    if (_contact) console.log("Se guardaron datos del contacto '" + _contact.id + "'");
}

// Función de validación de campos de correo
function validateEmail(value) {

    // Expresión regular para validar campos de correo
    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!EMAIL_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

// Función de validación de campos de teléfono
function validatePhone(value) {

    // Expresión regular para validar campos de teléfono
    const PHONE_REGEXP = /^[0-9\-\+\(\)\s]+$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!PHONE_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

// Función de validación de campos alfanuméricos
function validateAlphanumeric(value) {

    // Expresión regular para validar campos alfanuméricos
    const ALPHA_REGEXP = /^[a-zA-Z\-\_ñÑàèìòùáéíóúäëïöüÀÈÌÒÙÁÉÍÓÚÄËÏÖÜ\.0-9\-\+\(\)\s]+$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!ALPHA_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

function validateDate(value) {

  var d = formatDate(value);
  // Expresión regular para validar campos alfanuméricos
  const DateREGXP = /^(\d{4})-(\d{1,2})-(\d{1,2})/;

  // Si no se cumple la expresión, la validación no es correcta
  if (!DateREGXP.test(d)) return false;

  // La validación es correcta
  return true;
}

function formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  return [year, month, day].join('-');
}
///////////////////////////////////////
// Funciones para el WS //
///////////////////////////////////////

function actualizarContacto(_wsUrl, _contact) {
  console.log("Actualizar contacto: '" + _contact.id + "'");
//   console.log(JSON.stringify(_contact))
  var res = "KO";
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "text/plain");
  var contactCustomData = {};

  try {
    // Parseo de datos personalizados del contacto
    contactCustomData = JSON.parse(_contact.customData);    
} catch (err) {
    // Ocurrio un error al hacer el parse
    console.error("Ocurrio un error al obtener datos personalizados del contacto.");
    console.error(err);
}
  

  var rawJson = {
    "accountid": _contact.externalid,
    "eve_nombreapellidos": _contact.firstname + " " + _contact.lastname,
    "eve_nombre": _contact.firstname,
    "eve_apellidos": _contact.lastname,
    "eve_nif_nie": _contact.eve_nif_nie,
    "eve_telefono1": _contact.phone,
    // "eve_telefono2": contactCustomData.Telefono2,
    "eve_email": _contact.email,
    "eve_deudatotalinquilino": parseFloat(contactCustomData.eve_deudatotalinquilino),
    "eve_deudatotalcontrato": parseFloat(contactCustomData.eve_deudatotalcontrato),
    "eve_deudatotalplandepago": parseFloat(contactCustomData.eve_deudatotalplandepago),
    "eve_deudatotalanticipo": parseFloat(contactCustomData.eve_deudatotalanticipo),
    "eve_gestion_de_impagos": _contact.eve_gestion_de_impagos,
    "eve_fecha_cita": _contact.eve_fecha_cita,
    "eve_gestora_deuda": _contact.eve_gestora_deuda,
    "eve_fecha_promesa_pago": _contact.eve_fecha_promesa_pago,
    "eve_observaciones_deuda": _contact.eve_observaciones_deuda,
    "eve_envio_carta_deuda": _contact.eve_envio_carta_deuda,
    "eve_estado_anterior_deuda" : _contact.eve_estado_anterior_deuda,
    "eve_estado_deuda": _contact.eve_estado_deuda,
    "eve_observacion_carta": _contact.eve_observacion_carta,
    "eve_observacion_gestion":_contact.eve_observacion_gestion,
    "eve_multiplessociedades":_contact.eve_multiplessociedades  
  };

  if (rawJson.eve_multiplessociedades == ''){
    delete rawJson.eve_multiplessociedades
  }
  var fechaLimite = "1900-01-01";
  console.log("eve_fecha_cita " + rawJson.eve_fecha_cita)
  var fcontrol = new Date(fechaLimite);
  var f = new Date(rawJson.eve_fecha_cita);
  if (fcontrol > f){
    console.log("se borra del JSON eve_fecha_cita")
    delete rawJson.eve_fecha_cita
  }
  f = new Date(rawJson.eve_fecha_promesa_pago);
  console.log("eve_fecha_promesa_pago " + rawJson.eve_fecha_promesa_pago)
  if (fcontrol > f){
    console.log("se borra del JSON eve_fecha_promesa_pago")
    delete rawJson.eve_fecha_promesa_pago
  }

  var raw = JSON.stringify(rawJson);
  
  console.log(raw);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
    mode: 'cors'
  };
  
  fetch(_wsUrl + "/api/CRM/inquilino_update", requestOptions)
  .then(function(response) {
      return response.text();
    })
    .then(function(text) {
      console.log('Request ', text);
      if(text != "OK"){
        notificationsService.warn("Atención", "Error al enviar los datos a Dynamics " + text);
      }
      res = text;
    })
    .catch(function(error) {
      console.log('Request failed', error)
      notificationsService.warn("Atención", "Error al enviar los datos a Dynamics");
    });
  return res;
}

function RefreshContactFromCRM(id){
  var requestOptions = {
    method: 'GET',
    redirect: 'follow',
    mode: 'cors'
  };
  
  fetch(_wsUrl + "/api/CRM/GetInquilino?id='"+ id +"'", requestOptions)
    .then(response => response.text())
    .then(function(text) {
      console.log('Request ', text);
      if(text == "KO"){
        notificationsService.warn("Atención", "Error al enviar los datos a Dynamics " + text);
      }
      res = text;
    })
    .catch(function(error) {
      console.log('Request failed', error)
      notificationsService.warn("Atención", "Error al enviar los datos a Dynamics");
    });
}
