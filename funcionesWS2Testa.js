  //Necesarios para el WS
  var actualizar = false;
  var wsUrl = "https://5d605.inconcertcc.com:10443/";

//codigo dentro del onContactPropertyChanged
  // Para actualizar el contacto en su CRM
  actualizar = true;
  console.log("La información del contacto se actualizará al guardar: " + actualizar);

//codigo detro del onBeforeSave
  // Para actualizar el contacto en su CRM
  if (actualizar){
    actualizarContacto(wsUrl, contact, contactCustomData);
  }


function getToken(_wsUrl){
    var token = "KO";
    var requestOptions = {
        method: 'GET',
        redirect: 'follow',
        mode: 'no-cors'
      };
      
      fetch(_wsUrl+ "api/CRM/GetCRM", requestOptions)
      .then(function(response) {
        return response.text();
      })
      .then(function(text) {
        console.log('Request successful', text);
        token = text;
      })
      .catch(function(error) {
        console.log('Request failed', error)
      });
      return token;
}
function crearContacto_lead(_wsUrl, _contact, _contactCustomData){
    console.log("crearContacto_lead");
    var _externalid = "";
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");

    var raw = JSON.stringify({
        "eve_name": _contact.id,
        "eve_origen": _contactCustomData.eve_origen,
        "eve_tipo_inmueble": _contactCustomData.eve_tipo_inmueble,
        "eve_clasificacion": _contactCustomData.eve_clasificacion,
        "eve_nombre": _contact.firstname,
        "eve_apellidos": _contact.lastname,
        "eve_documento_identidad": _contactCustomData.eve_nif_nie,
        "eve_telefono": _contact.phone,
        "eve_email": _contact.email,
        "eve_codigo_postal": _contactCustomData.eve_codigopostal,
        "eve_profesion": _contactCustomData.eve_profesion,
        "eve_estado_civil": _contactCustomData.eve_estado_civil,
        "eve_empadronado": _contactCustomData.eve_empadronado,
        "eve_vivienda_propiedad": _contactCustomData.eve_vivienda_propiedad,
        "eve_ingresos_mensuales": _contactCustomData.eve_ingresos_mensuales,
        "eve_observaciones": _contactCustomData.eve_observaciones,
        "eve_campanya": _contactCustomData.eve_campanya,
        "eve_como_conocido": _contactCustomData.eve_como_conocido,
        "eve_zona_localidad": _contactCustomData.eve_zona_localidad,
        "eve_politica_proteccion_datos": _contactCustomData.eve_politica_proteccion_datos,
        "eve_info_comercial_empresa_colaboradora": _contactCustomData.eve_info_comercial_empresa_colaboradora,
        "eve_info_comercial_empresa_grupo": _contactCustomData.eve_info_comercial_empresa_grupo,
        "eve_info_testa_finalizado_contrato": _contactCustomData.eve_info_testa_finalizado_contrato
      });

    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
    mode: 'cors'
    };

    fetch(_wsUrl + "/api/CRM/contacto-lead-nuevo", requestOptions)
    .then(function(response) {
        //return response.text();
      })
      .then(function(text) {
        console.log('Externalid', text);
        _externalid = text;
      })
      .catch(function(error) {
        console.log('Request failed', error)
      });
  
    console.log(_externalid);
    return _externalid;
}
function crearCampanyaComercial(_wsUrl, _contact, _contactCustomData){
    console.log("crearCampanyaComercial");
    var _externalid = "";
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
    "eve_name": _contacdt_eve_name, //Fecha?
    "eve_tipo_envio_renovacion": 100000005,
    "eve_fecha_envio_renovacion_historico": "2020-10-06T08:21:24Z",
    "eve_fecha_fin_contrato": null,
    "eve_tipo_recuperacion": 100000000,
    "eve_motivo_inquilino": 100000000,
    "eve_equipo_encargado_recuperacion": 100000002,
    "eve_fecha_contacto": "2020-10-08T13:12:21Z",
    "eve_reasignacion": 100000001,
    "eve_otros_motivos_i": null,
    "eve_otros_motivos_ii": null,
    "eve_poblacion_destino": null,
    "eve_codigo_postal_destino": null,
    "eve_comentarios_motivo_finalizacion": "fchbxcvnbcv",
    "eve_estado_recuperacion": 100000007,
    "eve_fecha_finalizacion_recuperacion": null,
    "eve_comentarios_estado_recuperacion": "dfhdfgh",
    "eve_cierre_expediente": null,
    "eve_inquilino_recuperacion_1": null,
    "eve_email_recuperacion_inquilino_1": null,
    "eve_telefono_recuperacion_1": null,
    "eve_email_recuperacion_inquilino_2": null,
    "eve_inquilino_recuperacion_2": null,
    "eve_telefono_recuperacion_2": null,
    "eve_inquilino_recuperacion_3": null,
    "eve_email_recuperacion_inquilino_3": null,
    "eve_telefono_recuperacion_3": null,
    "eve_clasificacion_cambio_vivienda": null,
    "eve_motivo_no_recuperacion_1": null,
    "eve_motivo_no_recuperacion_2": null
    });

    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
    mode: 'cors'
    };

    fetch(wsUrl + "/api/CRM/campanya-comercial-nueva", requestOptions)
    .then(function(response) {
        //return response.text();
      })
      .then(function(text) {
        console.log('Externalid', text);
        _externalid = text;
      })
      .catch(function(error) {
        console.log('Request failed', error)
      });
    return _externalid;
}
function addExternalid2CustomData(_contact, _externalid){
  try {
      // Parseo de datos personalizados del contacto
      _contactCustomData = JSON.parse(_contact.customData);    
  } catch (err) {
      // Ocurrio un error al hacer el parse
      console.error("Ocurrio un error al obtener datos personalizados del contacto.");
      console.error(err);
  }
  _contactCustomData.externalid = _externalid;
  _contact.customData = JSON.stringify(_contactCustomData)
  console.log("externalid añadido al customdata")
}
function actualizarContacto(_wsUrl, _contact, _contactCustomData) {
  console.log("actualizar contacto: '" + _contact.id + "'");
  var res = "KO";
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "text/plain");
  
  var raw = JSON.stringify({
    "accountid": _contact.id,
    "eve_nombreapellidos": _contact.firstname + " " + _contact.lastname,
    "eve_nombre": _contact.firstname,
    "eve_apellidos": _contact.lastname,
    "eve_nif_nie": _contactCustomData.eve_nif_nie,
    "eve_telefono1": _contact.phone,
    "eve_telefono2": _contact.phoneLocal,
    "eve_telefono3": _contact.phoneInternational,
    "eve_email": _contact.email,
    "eve_tipovia": _contact.address1,
    "eve_calle": _contact.address2,
    "eve_numero": _contactCustomData.eve_numero,
    "eve_portal": _contactCustomData.eve_portal,
    "eve_escalera": _contactCustomData.eve_escalera,
    "eve_piso": _contactCustomData.eve_piso,
    "eve_puerta": _contactCustomData.eve_puerta,
    "eve_codigopostal": _contactCustomData.eve_codigopostal,
    "eve_poblacion": _contactCustomData.eve_poblacion,
    "eve_consentimiento_grupo": _contactCustomData.eve_consentimiento_grupo,
    "eve_consentimiento_terceras": _contactCustomData.eve_consentimiento_terceras,
    "eve_origen_consentimiento": _contactCustomData.eve_origen_consentimiento,
    "eve_fecha_consentimiento": new Date(_contactCustomData.eve_fecha_consentimiento),
    "eve_calificativo": _contactCustomData.eve_calificativo,
    "eve_descripcioncalificativo": _contactCustomData.eve_descripcioncalificativo,
    "eve_deudatotalinquilino": _contactCustomData.eve_deudatotalinquilino,
    "eve_deudatotalcontrato": _contactCustomData.eve_deudatotalcontrato,
    "eve_deudatotalplandepago": _contactCustomData.eve_deudatotalplandepago,
    "eve_deudatotalanticipo": _contactCustomData.eve_deudatotalanticipo,
    "eve_importe_total_pendiente_vencimiento_plan": _contactCustomData.eve_importe_total_pendiente_vencimiento_plan,
    "eve_gestion_de_impagos": _contactCustomData.eve_gestion_de_impagos,
    "eve_fecha_cita": new Date(_contactCustomData.eve_fecha_cita),
    "eve_gestora_deuda": _contactCustomData.eve_gestora_deuda,
    "eve_fecha_promesa_pago": new Date(_contactCustomData.eve_fecha_promesa_pago),
    "eve_observaciones_deuda": _contactCustomData.eve_observaciones_deuda,
    "eve_envio_carta_deuda": _contactCustomData.eve_envio_carta_deuda,
    "eve_observacion_carta": _contactCustomData.eve_observacion_carta
  });
  
  console.log(raw);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
    mode: 'cors'
  };
  
  fetch(_wsUrl + "/api/CRM/inquilino_update", requestOptions)
  .then(function(response) {
      return response.text();
    })
    .then(function(text) {
      console.log('Request successful', text);
      res = text;
    })
    .catch(function(error) {
      console.log('Request failed', error)
    });
  return res;
}


function addExternalid2CustomData(_contact, _externalid){
  try {
      // Parseo de datos personalizados del contacto
      _contactCustomData = JSON.parse(_contact.customData);    
  } catch (err) {
      // Ocurrio un error al hacer el parse
      console.error("Ocurrio un error al obtener datos personalizados del contacto.");
      console.error(err);
  }
  _contactCustomData.externalid = _externalid;
  _contact.customData = JSON.stringify(_contactCustomData)
  console.log("externalid añadido al customdata")
}
function editEve_nombreapellidos(_contact){
  console.log("editEve_nombreapellidos");
  try {
      // Parseo de datos personalizados del contacto
      var contactCustomData = JSON.parse(_contact.customData);    
  } catch (err) {
      // Ocurrio un error al hacer el parse
      console.error("Ocurrio un error al obtener datos personalizados del contacto.");
      console.error(err);
  }
  console.log("Original: " + contactCustomData.eve_nombreapellidos);
  contactCustomData.eve_nombreapellidos = _contact.firstname + " " + _contact.lastname;
  console.log("Cambiado: " + contactCustomData.eve_nombreapellidos);
  contact.customData = JSON.stringify(contactCustomData);
  console.log(contactCustomData.eve_nombreapellidos + " añadido al customdata");
  console.log("contacto: " + JSON.stringify(contact))
}