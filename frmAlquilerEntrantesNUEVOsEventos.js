// Referencias a elementos del formulario
var contact;
var interaction;
var contactInputsById;
var notificationsService;

// Datos custom del contacto
var contactCustomData = {};

//Necesarios para el WS
var externalid = "";
var wsUrl = "https://5d605.inconcertcc.com:10443/";

// Evento de formulario inicializado
this.onReady = function() {

    // Obtener y guardar referencias a elementos del formulario
    contact = this.contact;
    interaction = this.interaction;
    contactInputsById = this.contactInputsById;
    notificationsService = this.notificationsService;

    // Si hay interacción programar un timer de 1 minuto
    if (interaction) setTimeout(function() {
        // Mostrar notificación de nivel info al usuario
        notificationsService.info("Información", "La interacción lleva 1 minuto.");
    }, 60000);

    // Si hay interacción programar un timer de 5 minutos
    if (interaction) setTimeout(function() {
        // Mostrar notificación de nivel info al usuario
        notificationsService.info("Información", "La interacción lleva 5 minutos.");
    }, 300000);
    
};

// Evento de contacto cargado
this.onLoaded = function(_contact) {

    // Hay que actualizar la referencia al objeto contacto
    contact = _contact;

    try {
        // Parseo de datos personalizados del contacto
        contactCustomData = JSON.parse(contact.customData);    
    } catch (err) {
        // Ocurrio un error al hacer el parse
        console.error("Ocurrio un error al obtener datos personalizados del contacto.");
        console.error(err);
    }

    // Escribir a log
    if (_contact) console.log("Se cargaron datos del contacto '" + _contact.id + "'");
    if (_contact.id == undefined){
        crearcontacto = true;
        console.log("Crear contacto '" + crearcontacto + "'");
    }
} 

// Evento de cambio en propiedad de contacto
this.onContactPropertyChanged = function(property, value, _contact) {

    console.log("La información del contacto se actualizará al guardar: " + actualizar);

    
    // Escribir a log
    console.log("Información", "La propiedad '" + property + "' cambió a valor '" + value + "'");
    
    
};

// Evento de antes de guardar
this.onBeforeSave = function(_contact, mustFinishInteraction) {

    // Flag de impedir guardado
    var preventSave = false;

    // Si el formulario muestra el firstname y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.firstname && _contact && _contact.firstname && !validateAlphanumeric(_contact.firstname)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El nombre ingresado para el contacto (" + _contact.firstname + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el lastname y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.lastname && _contact && _contact.lastname && !validateAlphanumeric(_contact.lastname)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El apellido ingresado para el contacto (" + _contact.lastname + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el email y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.email && _contact && _contact.email && !validateEmail(_contact.email)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El correo ingresado para el contacto (" + _contact.email + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el phone y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.phone && _contact && _contact.phone && !validatePhone(_contact.phone)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El teléfono ingresado para el contacto (" + _contact.phone + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }
    // Para actualizar el contacto en su CRM
   
    return !preventSave;
}

// Evento de despues de guardar
this.onAfterSave = function(_contact) {

    // Escribir a log
    if (_contact) console.log("Se guardaron datos del contacto '" + _contact.id + "'");


}
// Evento de despues de guardar
this.onAfterCreate = function(_contact){
    
    if (_contact) console.log("Se creó el contacto '" + _contact.id + "'");

    try {
        // Parseo de datos personalizados del contacto
        contactCustomData = JSON.parse(contact.customData);    
    } catch (err) {
        // Ocurrio un error al hacer el parse
        console.error("Ocurrio un error al obtener datos personalizados del contacto.");
        console.error(err);
    }
    crearContacto_lead(wsUrl,_contact,contactCustomData);
}

// Función de validación de campos de correo
function validateEmail(value) {

    // Expresión regular para validar campos de correo
    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!EMAIL_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

// Función de validación de campos de teléfono
function validatePhone(value) {

    // Expresión regular para validar campos de teléfono
    const PHONE_REGEXP = /^[0-9\-\+\(\)\s]+$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!PHONE_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

// Función de validación de campos alfanuméricos
function validateAlphanumeric(value) {

    // Expresión regular para validar campos alfanuméricos
    const ALPHA_REGEXP = /^[a-zA-Z\-\_ñÑàèìòùáéíóúäëïöüÀÈÌÒÙÁÉÍÓÚÄËÏÖÜ\.0-9\-\+\(\)\s]+$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!ALPHA_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}
///////////////////////////////////////
// Funciones para el WS //
///////////////////////////////////////
function actualizarContacto(_wsUrl, _contact, _contactCustomData) {
    console.log("actualizar contacto: '" + _contact.id + "'");
    var res = "KO";
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    
    var rawJson = {
      "accountid": _contactCustomData.externalid,
      "eve_nombreapellidos": _contact.firstname + " " + _contact.lastname,
      "eve_nombre": _contact.firstname,
      "eve_apellidos": _contact.lastname,
      "eve_nif_nie": _contactCustomData.eve_nif_nie,
      "eve_telefono1": _contact.phone,
      // "eve_telefono2": _contactCustomData.Telefono2,
      "eve_email": _contact.email,
      "eve_deudatotalinquilino": parseFloat(_contactCustomData.eve_deudatotalinquilino),
      "eve_deudatotalcontrato": parseFloat(_contactCustomData.eve_deudatotalcontrato),
      "eve_deudatotalplandepago": parseFloat(_contactCustomData.eve_deudatotalplandepago),
      "eve_deudatotalanticipo": parseFloat(_contactCustomData.eve_deudatotalanticipo),
      "eve_gestion_de_impagos": _contactCustomData.eve_gestion_de_impagos,
      "eve_gestion_de_impagos_adicional": _contactCustomData.eve_gestion_de_impagos_adicional,
      "eve_fecha_cita": new Date(_contactCustomData.eve_fecha_cita),
      "eve_fecha_cita_adicional": new Date(_contactCustomData.eve_fecha_cita_adicional),
      "eve_gestora_deuda": _contactCustomData.eve_gestora_deuda,
      "eve_fecha_promesa_pago": new Date(_contactCustomData.eve_fecha_promesa_pago),
      "eve_observaciones_deuda": _contactCustomData.eve_observaciones_deuda,
      "eve_envio_carta_deuda": _contactCustomData.eve_envio_carta_deuda,
      "eve_estado_anterior_deuda" : _contactCustomData.eve_estado_anterior_deuda,
      "eve_estado_deuda": _contactCustomData.eve_estado_deuda,
      "eve_observacion_carta": _contactCustomData.eve_observacion_carta,
      "eve_observacion_gestion":_contactCustomData.eve_observacion_gestion,
      "eve_multiplessociedades":_contactCustomData.eve_multiplessociedades  
    };
  
    if (_contactCustomData.eve_multiplessociedades == ''){
      delete rawJson.eve_multiplessociedades
    }
    var fechaLimite = new Date("1900-01-01");
    if (rawJson.eve_fecha_cita < fechaLimite){
        delete rawJson.eve_fecha_cita
    }
    if (rawJson.eve_fecha_cita_adicional < fechaLimite){
      delete rawJson.eve_fecha_cita_adicional
      }
  if (rawJson.eve_fecha_promesa_pago < fechaLimite){
      delete rawJson.eve_fecha_promesa_pago
      }
  
    var raw = JSON.stringify(rawJson);
    
    console.log(raw);
  
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
      mode: 'cors'
    };
    
    fetch(_wsUrl + "/api/CRM/inquilino_update", requestOptions)
    .then(function(response) {
        return response.text();
      })
      .then(function(text) {
        console.log('Request successful', text);
        if(text != "OK"){
          notificationsService.warn("Atención", "Error al enviar los datos a Dynamics" + text);
        }
        res = text;
      })
      .catch(function(error) {
        console.log('Request failed', error)
        notificationsService.warn("Atención", "Error al enviar los datos a Dynamics");
      });
    return res;
  }

function crearContacto_lead(_wsUrl, _contact, _contactCustomData){
    console.log("crearContacto_lead");
    var _externalid = "";
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");

    var raw = JSON.stringify({
        "eve_name": _contact.id,
        "eve_origen": _contactCustomData.eve_origen,
        "eve_tipo_inmueble": _contactCustomData.eve_tipo_inmueble,
        "eve_clasificacion": _contactCustomData.eve_clasificacion,
        "eve_nombre": _contact.firstname,
        "eve_apellidos": _contact.lastname,
        "eve_documento_identidad": _contactCustomData.eve_nif_nie,
        "eve_telefono": _contact.phone,
        "eve_email": _contact.email,
        "eve_codigo_postal": _contactCustomData.eve_codigopostal,
        "eve_profesion": _contactCustomData.eve_profesion,
        "eve_estado_civil": _contactCustomData.eve_estado_civil,
        "eve_empadronado": _contactCustomData.eve_empadronado,
        "eve_vivienda_propiedad": _contactCustomData.eve_vivienda_propiedad,
        "eve_ingresos_mensuales": _contactCustomData.eve_ingresos_mensuales,
        "eve_observaciones": _contactCustomData.eve_observaciones,
        "eve_campanya": _contactCustomData.eve_campanya,
        "eve_como_conocido": _contactCustomData.eve_como_conocido,
        "eve_zona_localidad": _contactCustomData.eve_poblacion,
        "eve_politica_proteccion_datos": _contactCustomData.eve_politica_proteccion_datos,
        "eve_info_comercial_empresa_colaboradora": _contactCustomData.eve_info_comercial_empresa_colaboradora,
        "eve_info_comercial_empresa_grupo": _contactCustomData.eve_info_comercial_empresa_grupo,
        "eve_info_testa_finalizado_contrato": _contactCustomData.eve_info_testa_finalizado_contrato,
        "eve_promocion_id": _contactCustomData.eve_promocion_id
      });

    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
    mode: 'cors'
    };

    fetch(_wsUrl + "/api/CRM/contacto-lead-nuevo", requestOptions)
    .then(function(response) {
        console.log(response.text());
      })
      .then(function(text) {
        console.log('Externalid', text);
        _externalid = text;
      })
      .catch(function(error) {
        console.log('Request failed', error)
      });
  
    console.log(_externalid);
    return _externalid;
}

function addExternalid2CustomData(_contact, _externalid){
    // try {
    //     // Parseo de datos personalizados del contacto
    //     _contactCustomData = JSON.parse(_contact.customData);    
    // } catch (err) {
    //     // Ocurrio un error al hacer el parse
    //     console.error("Ocurrio un error al obtener datos personalizados del contacto.");
    //     console.error(err);
    // }
    // _contactCustomData.externalid = _externalid;
    // _contact.customData = JSON.stringify(_contactCustomData)
    // console.log("externalid añadido al customdata")
  }