// Referencias a elementos del formulario
var contact;
var interaction;
var contactInputsById;
var notificationsService;
// Necesario refresco contactos via WS
var component = this;
var firsttime = true;
var actualizar = false;
//URL WS de PRE
var wsUrl = "https://5d605pre.inconcertcc.com:10545";
// URL WS de pro
//var wsUrl = "https://5d605.inconcertcc.com:10443";

///
var contactEvents = [];
// Datos custom del contacto
var contactCustomData = {};

// Datos asociados a la tabla
this.tableLoading = false;
this.tableData = [];
this.tableHtml = '';
this.selectedData = {};
this.selectedHtml = '';

// Evento de formulario inicializado
this.onReady = function() {

    // Obtener y guardar referencias a elementos del formulario
    contact = this.contact;
    interaction = this.interaction;
    contactInputsById = this.contactInputsById;
    notificationsService = this.notificationsService;

    // Si hay interacción programar un timer de 1 minuto
    if (interaction) setTimeout(function() {
        // Mostrar notificación de nivel info al usuario
        notificationsService.info("Información", "La interacción lleva 1 minuto.");
    }, 60000);

    // Si hay interacción programar un timer de 5 minutos
    if (interaction) setTimeout(function() {
        // Mostrar notificación de nivel info al usuario
        notificationsService.info("Información", "La interacción lleva 5 minutos.");
    }, 300000);
};

// Evento de contacto cargado
this.onLoaded = function(_contact) {

    // Hay que actualizar la referencia al objeto contacto
    contact = _contact;
    
    if (!this.contact.country) this.contact.country = 'ES';
 
   
    // Consulto lista de eventos del contacto
    this.contactService.GetContactEvents(contact.id).subscribe(
        result => { 
            // Guardo el dato obtenido
            contactEvents = result && Array.isArray(result) ? result : [];
           // console.log(contactEvents);
        }
    );
   

    // Llamada al WS para actualizar el contacto
    var requestOptions = {
        method: 'GET',
        redirect: 'follow',
        mode: 'cors'
    };

    fetch(wsUrl + "/api/CRM/GetInquilino?id='" + contact.id + "'", requestOptions)
        .then(function (response) {
        response.json()
        .then(function (OK) {
        var contact_crm = OK[0];

        try {
          contactCustomData = JSON.parse(contact.customData);
        } catch (err) {
          console.error("Ocurrio un error al obtener datos personalizados del contacto.");
          console.error(err);
        }
        if (contact_crm != undefined) {
          contact = mergeContacts(contact, contact_crm, contactCustomData);
          actualizar = true;
        }

        if (firsttime) {
          component.InitializeContact();
          firsttime = false;
        }

        try {
          contactCustomData = JSON.parse(contact.customData);
        } catch (err) {
          console.error("Ocurrio un error al obtener datos personalizados del contacto.");
          console.error(err);
        }
        // Escribir a log
        if (contact) console.log("Se cargaron datos del contacto '" + contact.id + "'");
        
        //Oculto campos de preguntas
        ocultarPreguntas();
        //Oculto campos de campañas comerciales
        ocultarCamposCC();
        $("#tablaContratos").hide();

      })
      .catch(function (KO) {
        console.log('Request failed: ', KO);
        notificationsService.warn("Atención", "Error al cargar los datos de Dynamics: " + KO);
      })
    }).catch(function (error) {
      console.log('Request failed: ', error)
      notificationsService.warn("Atención", "Error al cargar los datos de Dynamics: " + error);
    }); 

}

// Evento de cambio en propiedad de contacto
this.onContactPropertyChanged = function(property, value, _contact) {
    
    // Escribir a log
    console.log("Información", "La propiedad '" + property + "' cambió a valor '" + value + "'");
    
    if(property == 'PreguntaP1') {
        ocultarPreguntas();
        if(value == 'No'){
              mostrarPregunta(2);
        }else if (value == 'Tengo dudas'){
              mostrarPregunta(13);
        }else{
            ocultarPreguntas();
        }
    }
    
    if(property == 'PreguntaP2') {
        if (value == 'Me voy a comprar una vivienda'){
            ocultarPregunta(3);
            ocultarPregunta(4);
            ocultarPregunta(5);
            ocultarPregunta(6);
            ocultarPregunta(7);
            ocultarPregunta(8);
            ocultarPregunta(9);
            ocultarPregunta(10);
            ocultarPregunta(11);
            ocultarPregunta(12);
            ocultarPregunta(13);
            ocultarPregunta(14);
        }else if (value == 'Porque tengo que cuidar a un familiar'){
            ocultarPregunta(3);
            ocultarPregunta(4);
            ocultarPregunta(5);
            ocultarPregunta(6);
            ocultarPregunta(7);
            ocultarPregunta(8);
            ocultarPregunta(9);
            ocultarPregunta(10);
            ocultarPregunta(11);
            ocultarPregunta(12);
            ocultarPregunta(13);
            ocultarPregunta(14);
        }else if(value == 'Me voy a vivir con mis padres'){
            mostrarPregunta(3);

            ocultarPregunta(4);
            ocultarPregunta(5);
            ocultarPregunta(6);
            ocultarPregunta(7);
            ocultarPregunta(8);
            ocultarPregunta(9);
            ocultarPregunta(10);
            ocultarPregunta(11);
            ocultarPregunta(12);
            ocultarPregunta(13);
            ocultarPregunta(14);
        } else if(value == 'Me voy a una más barata'){
            mostrarPregunta(4); 
            mostrarPregunta(5); 
            mostrarPregunta(6); 
            mostrarPregunta(9); 
              
            ocultarPregunta(3);
            ocultarPregunta(7);
            ocultarPregunta(8);
            ocultarPregunta(10);
            ocultarPregunta(11);
            ocultarPregunta(13);
            ocultarPregunta(12);
            ocultarPregunta(14);
              
        }else if(value == 'Porque voy a cambiar de trabajo'){
              mostrarPregunta(4);
              mostrarPregunta(5);
              mostrarPregunta(6);
              
            ocultarPregunta(3);
            ocultarPregunta(7);
            ocultarPregunta(8);
            ocultarPregunta(9);
            ocultarPregunta(10);
            ocultarPregunta(11);
            ocultarPregunta(12);
            ocultarPregunta(13);
            ocultarPregunta(14);
             
        }else if(value == 'Necesito otro tipo de vivienda'){
            mostrarPregunta(4);
              
            ocultarPregunta(3);
            ocultarPregunta(5);
            ocultarPregunta(6);
            ocultarPregunta(7);
            ocultarPregunta(8);
            ocultarPregunta(9);
            ocultarPregunta(10);
            ocultarPregunta(11);
            ocultarPregunta(12);
            ocultarPregunta(13);
            ocultarPregunta(14);
            
        }else if(value == 'Falta de satisfacción en la resolución de incidencias'){
            mostrarPregunta(7);
            mostrarPregunta(8);
            
            ocultarPregunta(3);
            ocultarPregunta(4);
            ocultarPregunta(5);
            ocultarPregunta(6);
            ocultarPregunta(9);
            ocultarPregunta(10);
            ocultarPregunta(11);
            ocultarPregunta(12);
            ocultarPregunta(13);
            ocultarPregunta(14);
        }else if(value == 'Otros motivos'){
            mostrarPregunta(14);
            
            ocultarPregunta(3);
            ocultarPregunta(4);
            ocultarPregunta(5);
            ocultarPregunta(6);
            ocultarPregunta(7);
            ocultarPregunta(8);
            ocultarPregunta(9);
            ocultarPregunta(10);
            ocultarPregunta(11);
            ocultarPregunta(12);
            ocultarPregunta(13);
        }
    }
    
    if(property == 'PreguntaP3') {
        if(value == 'Sí'){
            mostrarPregunta(4);
            ocultarPregunta(7);
        }else if(value == 'No'){
            mostrarPregunta(7);
            ocultarPregunta(4);
            ocultarPregunta(10);
            ocultarPregunta(11);
            ocultarPregunta(12);
        }else{
            ocultarPregunta(4);
            ocultarPregunta(7);
            ocultarPregunta(10);
            ocultarPregunta(11);
            ocultarPregunta(12);
        }
    }
    
    if(property == 'PreguntaP4') {
        if(value == 'Sí'){
            mostrarPregunta(10);
            mostrarPregunta(11);
            mostrarPregunta(12);
        }else{
            ocultarPregunta(10);
            ocultarPregunta(11);
            ocultarPregunta(12);
        }
    }
    
    //Campañas comerciales
    if(property == 'campana_comercial') {
        if(value == 'SI'){
            mostrarCamposCC(1);
            var btn = component.document.getElementById("btnGuardarCC");
            if(!btn){
              CrearBotonGuardarCC();
            }
            else{
              $("#btnGuardarCC").show();
            }
        }else{
            ocultarCamposCC();
            $("#btnGuardarCC").hide();
        }
    }
   
};

// Evento de antes de guardar
this.onBeforeSave = function(_contact, mustFinishInteraction) {

    // Flag de impedir guardado
    var preventSave = false;

    // Si el formulario muestra el firstname y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.firstname && _contact && _contact.firstname && !validateAlphanumeric(_contact.firstname)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El nombre ingresado para el contacto (" + _contact.firstname + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el lastname y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.lastname && _contact && _contact.lastname && !validateAlphanumeric(_contact.lastname)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El apellido ingresado para el contacto (" + _contact.lastname + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el email y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.email && _contact && _contact.email && !validateEmail(_contact.email)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El correo ingresado para el contacto (" + _contact.email + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el phone y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.phone && _contact && _contact.phone && !validatePhone(_contact.phone)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El teléfono ingresado para el contacto (" + _contact.phone + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }
    
       //Comprobar codigo campaña comercial introducido
    if(!preventSave && contact.campana_comercial == 'SI' && !_contact.codigo_campanya_comercial){

      notificationsService.warn("Atención", "Debe ingresar Código campaña comercial");
        // Interrumpir el guardado
        preventSave = true;
    }
    
    /* Descomentar en cuanto se cree la campaña comercial desde formulario.
    //Crear campaña comercial , llamada al WS
    if(!preventSave && contact.campana_comercial == 'SI'){

      crearCampanyaComercial(wsUrl, _contact);
    }
    */
    
    // Permitir el guardado
    return !preventSave;
}

// Evento de despues de guardar
this.onBeforeSaveDisposition = function(_contact, mustFinishInteraction) {
    
    // Escribir a log
    if (_contact){ 
        //console.log(this.occApiInteractionDisposition);
       // console.log("--------------------------------------------");
        //console.log(this.occApiInteractionDispositions);
        //console.log("La disposicione es Goal: "+ this.occApiInteractionDisposition.isGoal);
       // console.log(this.occApiInteractionDisposition.__treePath);
        if(this.occApiInteractionDisposition != undefined){
          if (!this.occApiInteractionDisposition.isGoal){
           
            if (checkMaxNotAnswereds(contactEvents, this.occApiInteractionDisposition.name)){
                console.log("Cambiar tipificacion de llamada y ETAPA");
                this.setContactProperty("stage","NegativoNoLocalizable");
                this.setContactProperty("notificacion_reactivacion", "NegativoNoLocalizable");
                _contact.customData = this.GenerateCustomData();
                
               this.occApiInteractionDisposition.code = '41';
               this.occApiInteractionDisposition.name = 'Supera número de llamadas';
               this.occApiInteractionDisposition.__treePath[0]='Ausente';
               this.occApiInteractionDisposition.__treePath[1]='Supera número de llamadas';  
               
               
                
     
               
        //        /* Probar mañana y sustituir lineas 372 a 375
               
        //        var forceDisposition;
                
        //         // Nombre del disposition que se busca
        //         var forceDispositionName = "Supera número de llamadas";
                
        //         // Función recursiva para obuscar un resultado con el nombre indicado
        //         var lookForDisposition = function(dispositions) {
        //             // Recorro la lista de resultados recibida
        //             if (dispositions && dispositions.length) dispositions.forEach(
        //                 disposition => {
        //                     // Si ya se encontró el elemento buscado o no se recibió dato, no sigo
        //                     if (forceDisposition || !disposition) return;
        //                     // Verifico si el elemento actual es final y su nombre coinide con el buscado
        //                     if (disposition && !disposition.childs && disposition.name === forceDispositionName) {
        //                         // Guardo la referencia
        //                         forceDisposition = disposition;
        //                         // No sigo
        //                         return;
        //                     }
        //                     // Si el elemento es una carpeta aplico recursividad
        //                     if (disposition.childs && Array.isArray(disposition.childs)) lookForDisposition(disposition.childs);
        //                 }
        //             );
        //         };
                
        //         // Ejecuto la función para el nivel inicial de resultados
        //         lookForDisposition(this.occApiInteractionDispositions);
                
        //         // Si se obtuvo un disposition para forzar, piso el seleccionado
        //         if (forceDisposition) this.occApiInteractionDisposition = forceDisposition;
                
        //         */
               
            }
          }
        }
        
    }
}

// Evento de despues de guardar
this.onAfterSave = function(_contact) {

    // Escribir a log
    if (_contact) console.log("Se guardaron datos del contacto '" + _contact.id + "'");
}


//#####################################################################################//
//#                 Funciones de Validacion de campos                                 #//
//#####################################################################################//

// Función de validación de campos de correo
function validateEmail(value) {

    // Expresión regular para validar campos de correo
    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!EMAIL_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

// Función de validación de campos de teléfono
function validatePhone(value) {

    // Expresión regular para validar campos de teléfono
    const PHONE_REGEXP = /^[0-9\-\+\(\)\s]+$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!PHONE_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

// Función de validación de campos alfanuméricos
function validateAlphanumeric(value) {

    // Expresión regular para validar campos alfanuméricos
    const ALPHA_REGEXP = /^[a-zA-Z\-\_ñÑàèìòùáéíóúäëïöüÀÈÌÒÙÁÉÍÓÚÄËÏÖÜ\.0-9\-\+\(\)\s]+$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!ALPHA_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}


//#####################################################################################//
//#                 Funciones verificar Intentos                                      #//
//#####################################################################################//
function isNotAnswered(dispositionName) {
    //console.log("Disposicion recibida: "+ dispositionName);
    
    const notAnsweredDispositions = ["NO_ANSWER", "ANSWERING_MACHINE","BUSY","No contesta","Buzón de voz","Comunica","Estoy ocupado","No está ahora"];
    return notAnsweredDispositions.includes(dispositionName);
}

function checkMaxNotAnswereds(contactEvents, currentDisposition) {
    const DAYS_TO_CHECK = 30;
    const MAX_NOT_ANSWEREDS = 4;
    let checkDate = new Date(new Date().setDate(new Date().getDate()-DAYS_TO_CHECK));
    let notAnswereds = 0;

    let contactEventsFiltered = contactEvents.filter((e) => e.eventType == "call" && new Date(e?.eventDate) > checkDate);
   // console.log(contactEventsFiltered);
    for (let i = 1; i < contactEventsFiltered.length; i++) {
        let j = contactEventsFiltered[i];
        if (isNotAnswered(j.eventData5)) {
            notAnswereds ++;
            if (notAnswereds == MAX_NOT_ANSWEREDS){
                break;
            }
        } else {
            notAnswereds = 0;
            break;
        }
    }
    return notAnswereds >= MAX_NOT_ANSWEREDS && isNotAnswered(currentDisposition);
}

//#####################################################################################//
//#                 Funciones de Preguntas y Campos                                   #//
//#####################################################################################//

// Función para ocultar las preguntas desplegables.
function ocultarPreguntas() {
        //Oculto campos de preguntas
        ocultarPregunta(2);
        ocultarPregunta(3);
        ocultarPregunta(4);
        ocultarPregunta(5);
        ocultarPregunta(6);
        ocultarPregunta(7);
        ocultarPregunta(8);
        ocultarPregunta(9);
        ocultarPregunta(10);
        ocultarPregunta(11);
        ocultarPregunta(12);
        ocultarPregunta(13);
        ocultarPregunta(14);
    return true;
}

//funcion para ocultar pregunta
function ocultarPregunta(value){
    if(value==2){
        $("label[for='customProperties_PreguntaP2']").text("");
        $("#customProperties_PreguntaP2").hide(); 
    }
    if(value==3){
        $("label[for='customProperties_PreguntaP3']").text("");
        $("#customProperties_PreguntaP3").hide();
    }
    if(value==4){
        $("label[for='customProperties_PreguntaP4']").text("");
        $("#customProperties_PreguntaP4").hide();
    }
    if(value==5){
        $("label[for='customProperties_PreguntaP5']").text("");
        $("#customProperties_PreguntaP5").hide();
    }
    if(value==6){
        $("label[for='customProperties_PreguntaP6']").text("");
        $("#customProperties_PreguntaP6").hide();
    }
    if(value==7){
       $("label[for='customProperties_PreguntaP7']").text("");
        $("#customProperties_PreguntaP7").hide(); 
    }
    if(value==8){
        $("label[for='customProperties_PreguntaP8']").text("");
        $("#customProperties_PreguntaP8").hide();
    }
    if(value==9){
        $("label[for='customProperties_PreguntaP9']").text("");
        $("#customProperties_PreguntaP9").hide();
    }
    if(value==10){
        $("label[for='customProperties_PreguntaP10']").text("");
        $("#customProperties_PreguntaP10").hide();
    }
    if(value==11){
        $("label[for='customProperties_PreguntaP11']").text("");
        $("#customProperties_PreguntaP11").hide();
    }
    if(value==12){
        $("label[for='customProperties_PreguntaP12']").text("");
        $("#customProperties_PreguntaP12").hide();
    }
    if(value==13){
        $("label[for='customProperties_PreguntaP13']").text("");
        $("#customProperties_PreguntaP13").hide();
    }
     if(value==14){
        $("label[for='customProperties_PreguntaP14']").text("");
        $("#customProperties_PreguntaP14").hide();
    }
}

// funcion para mostrar preguntas
function mostrarPregunta(value){
    if (value == 2){
        $("label[for='customProperties_PreguntaP2']").text("Motivos");
        $("#customProperties_PreguntaP2").show();
    }
    if (value== 3){
        $("label[for='customProperties_PreguntaP3']").text("¿Es la complicada situación actual el motivo por el que ha decidido dejar la vivienda?");
        $("#customProperties_PreguntaP3").show();
    }
    if (value==4){
        $("label[for='customProperties_PreguntaP4']").text("¿Quiere que le contacte nuestro departamento comercial para informarle?");
        $("#customProperties_PreguntaP4").show();
    }
    if (value==5){
        $("label[for='customProperties_PreguntaP5']").text("Población destino");
        $("#customProperties_PreguntaP5").show();
    }
    if (value==6){
        $("label[for='customProperties_PreguntaP6']").text("Código Postal destino");
        $("#customProperties_PreguntaP6").show();  
    }
    if (value==7){
        $("label[for='customProperties_PreguntaP7']").text("¿Le importaría decirme cuáles?");
        $("#customProperties_PreguntaP7").show();
    }
    if (value==8){
        $("label[for='customProperties_PreguntaP8']").text("Si le solucionaran estas incidencias ¿continuaría usted en la vivienda?");
        $("#customProperties_PreguntaP8").show();
    }
    if (value==9){
        $("label[for='customProperties_PreguntaP9']").text("¿Qué renta va a pagar en su nueva vivienda?");
        $("#customProperties_PreguntaP9").show();
    }
    if (value==10){
        $("label[for='customProperties_PreguntaP10']").text("¿En qué zona sería?");
        $("#customProperties_PreguntaP10").show();
    }
    if (value==11){
        $("label[for='customProperties_PreguntaP11']").text("¿Cuántos dormitorios necesitaría?");
        $("#customProperties_PreguntaP11").show();
    }
    if (value==12){
        $("label[for='customProperties_PreguntaP12']").text("¿Cuál es la renta mensual que podría pagar?");
        $("#customProperties_PreguntaP12").show();
    }
    if (value== 13){
        $("label[for='customProperties_PreguntaP13']").text("Tengo Dudas");
        $("#customProperties_PreguntaP13").show();
    }
     if (value== 14){
        $("label[for='customProperties_PreguntaP14']").text("Otros Motivos");
        $("#customProperties_PreguntaP14").show();
    }
}

// Funcion para mostrar campos de crear campaña comercial.
function mostrarCamposCC(value){
    
    if (value == 1){
      $("label[for='customProperties_eve_tipo_recuperacion']").text("Tipo recuperación");
      $("#customProperties_eve_tipo_recuperacion").show();
      
      $("label[for='customProperties_eve_motivo_inquilino']").text("Motivo inquilino");
      $("#customProperties_eve_motivo_inquilino").show();
      
      $("label[for='customProperties_eve_otros_motivos_i']").text("Otros Motivos I");
      $("#customProperties_eve_otros_motivos_i").show();
      
      $("label[for='customProperties_eve_otros_motivos_ii']").text("Otros Motivos II");
      $("#customProperties_eve_otros_motivos_ii").show();
      
      $("label[for='customProperties_eve_poblacion_destino']").text("Población destino");
      $("#customProperties_eve_poblacion_destino").show();
      
      $("label[for='customProperties_eve_codigo_postal_destino']").text("Código postal destino");
      $("#customProperties_eve_codigo_postal_destino").show();
      
      $("label[for='customProperties_eve_comentarios_motivo_finalizacion']").text("Comentarios motivo finalización");
      $("#customProperties_eve_comentarios_motivo_finalizacion").show();
      
      $("label[for='customProperties_eve_cierre_expediente']").text("Cierre de expediente (call center)");
      $("#customProperties_eve_cierre_expediente").show();
      
      $("label[for='customProperties_eve_inquilino_recuperacion_1']").text("Inquilino recuperación 1");
      $("#customProperties_eve_inquilino_recuperacion_1").show();
      
      $("label[for='customProperties_eve_email_recuperacion_inquilino_1']").text("Email inquilino recuperación 1");
      $("#customProperties_eve_email_recuperacion_inquilino_1").show();
      
      $("label[for='customProperties_eve_telefono_recuperacion_1']").text("Teléfono inquilino recuperación 1");
      $("#customProperties_eve_telefono_recuperacion_1").show();
      
      $("label[for='customProperties_codigo_campanya_comercial']").text("Código campaña comercial");
      $("#customProperties_codigo_campanya_comercial").show();
    }
    if (value==2){
      $("label[for='customProperties_codigo_campanya_comercial']").text("Código campaña comercial");
      $("#customProperties_codigo_campanya_comercial").show();
    }
}
// Funcion para ocultar campos de crear Campaña comercial.
function ocultarCamposCC(){
    //Oculto campos de campañas comerciales
        $("label[for='customProperties_eve_tipo_recuperacion']").text("");
        $("#customProperties_eve_tipo_recuperacion").hide();
        
        $("label[for='customProperties_eve_motivo_inquilino']").text("");
        $("#customProperties_eve_motivo_inquilino").hide();
        
        $("label[for='customProperties_eve_otros_motivos_i']").text("");
        $("#customProperties_eve_otros_motivos_i").hide();
        
        $("label[for='customProperties_eve_otros_motivos_ii']").text("");
        $("#customProperties_eve_otros_motivos_ii").hide();
        
        $("label[for='customProperties_eve_poblacion_destino']").text("");
        $("#customProperties_eve_poblacion_destino").hide();
        
        $("label[for='customProperties_eve_codigo_postal_destino']").text("");
        $("#customProperties_eve_codigo_postal_destino").hide();
        
        $("label[for='customProperties_eve_comentarios_motivo_finalizacion']").text("");
        $("#customProperties_eve_comentarios_motivo_finalizacion").hide();
        
        $("label[for='customProperties_eve_cierre_expediente']").text("");
        $("#customProperties_eve_cierre_expediente").hide();
        
        $("label[for='customProperties_eve_inquilino_recuperacion_1']").text("");
        $("#customProperties_eve_inquilino_recuperacion_1").hide();
        
        $("label[for='customProperties_eve_email_recuperacion_inquilino_1']").text("");
        $("#customProperties_eve_email_recuperacion_inquilino_1").hide();
        
        $("label[for='customProperties_eve_telefono_recuperacion_1']").text("");
        $("#customProperties_eve_telefono_recuperacion_1").hide();
        
        // $("label[for='customProperties_codigo_campanya_comercial']").text("");
        // $("#customProperties_codigo_campanya_comercial").hide();
    return true;
}
///////////////////////////////////////
// Funciones para manejo de la tabla //
///////////////////////////////////////

// Función para hacer la consulta de datos y actualizar la tabla
this.LoadTable = function() {
    
  // Verifico que no exista una consulta en proceso
  if (this.tableLoading) {
      // Mostrar notificación de nivel info al usuario
      notificationsService.warn("Atención", "Existe una consulta de datos en proceso.");
      // Devuelvo false
      return false;
  }

  // Limpio el detalle mostrado
  this.SetTableResultDetail(-1);

  // Paso la tabla a modo loading
  this.SetTableLoading();

  // Ejecuto consulta de inmuebles
  this.executeProcedure(
      "TESTA_GetParticipacionContratos",
      [
          { column: "CodigoInquilino", value: contactCustomData.name },
  
      ],
      function(error, recordset) {

          // Verifico qsi hubo error
          if (error) {
              // Muestro notificación de error
              notificationsService.error("Error", "Ocurrió un error al consultar los contratos del contacto.");
          }
          
          // Cargo resultados obtenidos en la tabla
          this.SetTableResults(recordset || []);

      }.bind(this)
  );
  
  // Devuelvo false
  return false;
}

// Función para cambiar la tabla a modo loading
this.SetTableLoading = function() {
  
  // Actualizo flag de tabla cargando
  this.tableLoading = true;

  // Cargo HTML de tabla en modo "cargando"
  this.BuildTable(`
      <tr>
          <td colspan="7">
              <div>
                  <i class="fas fa-spinner fa-spin loading-spinner"></i>
              </div>
          </td>
      </tr>
  `);

  // Refresco el HTML custom
  this.RefreshCustomHtml();
}

// Función para cambiar la tabla a modo sin resultados
this.SetTableNoResults = function() {
  
  // Actualizo flag de tabla cargando
  this.tableLoading = false;

  // Actualizo la variable de datos
  this.tableData = [];

  // Cargo HTML de tabla en modo "sin resultados"
  this.BuildTable(`
      <tr class="noRecordsRow">
          <td colspan="7">
              <div class="alert alert-info mt-0" role="alert">
                  <span class="fas fa-info"></span>
                  <strong> No se encontraron resultados </strong> 'Aún no hay elementos creados, o que coincidan con la búsqueda actual.'
              </div>
          </td>
      </tr>
  `);

  // Refresco el HTML custom
  this.RefreshCustomHtml();
}

// Función para cargar los resultados a la tabla
this.SetTableResults = function(data) {

  // Verifico si se obtuvieron resultados
  if (!data || !data.length) {
      // Paso la tabla a modo sin resultados
      this.SetTableNoResults();
      // No sigo
      return;
  }
  
  // Actualizo flag de tabla cargando
  this.tableLoading = false;

  // Actualizo la variable de datos
  this.tableData = data;

  // HTML de las filas
  var rowsHtml = '';

  // Recorro el array de datos y los agrego como filas del HTML
  data.forEach(
      (row, index) => rowsHtml = rowsHtml + `
          <tr class="cursor-pointer" onclick="this.SetTableResultDetail(${index})">
              <td>${row.CodigoContrato || ''}</td>
              <td>${row.NombreApellidos || ''}</td>
              <td>${row.Descripcion || ''}</td>
              <td>${row.EstadoContrato || ''}</td>
              <td>${row.TipoFinalizacion || ''}</td>
              <td>${row.FechaFin || ''}</td>
              <td>${row.FechaLiquidacion || ''}</td>
          </tr>`
  );

  // Cargo HTML de tabla en modo "con resultados"
  this.BuildTable(rowsHtml);

  // Refresco el HTML custom
  this.RefreshCustomHtml();
}

// Función para construir el HTML de la tabla
this.BuildTable = function(tableContent) {

// Cargo HTML de tabla en modo "con resultados"
  this.tableHtml = `
<table id="tableContracts" class="table table-sm table-striped table-hover table-bordered editableTable">
  <thead class="thead-inverse">
      <tr>
          <th style="width: 15%">CodigoContrato</th>
          <th style="width: 15%">NombreApellidos</th>
          <th style="width: 15%">Descripcion</th>
          <th style="width: 15%">EstadoContrato</th>
          <th style="width: 15%">TipoFinalizacion</th>
          <th style="width: 15%">FechaFin</th>
          <th style="width: 10%">FechaLiquidacion</th>
      </tr>
  </thead>
  <tbody>
      ${tableContent || ''}
  </tbody>
</table>
  `;
}

// Función para seleccionar un elemento y mostrar su detalle
this.SetTableResultDetail = function(index) {

  // Obtengo el dato seleccionado
  var row = this.tableData ? this.tableData[index] : null;

  // Verifico que se haya obtenido un dato
  if (!row) {
      // Limpio as variables
      this.selectedData = null;
      this.selectedHtml = '';
      // Refresco el HTML custom
      this.RefreshCustomHtml();
      // No sigo
      return;
  }

  // Guardo el elemento seleccionado
  this.selectedData = row;

  console.log("Fila selecionada: " + JSON.stringify(row));

  contact.eve_contratoid  = this.selectedData.ContratoId;    

  console.log("contact.eve_contratoid " + contact.eve_contratoid);

  var customDataAux = {}
        try {
            customDataAux = JSON.parse(contact.customData);
            customDataAux["eve_codigocontrato"] = this.selectedData.CodigoContrato;

            contact.customData = JSON.stringify(customDataAux);

        } catch (err) {
          console.error("Ocurrio un error al obtener datos personalizados del contacto.");
          console.error(err);
        }
  
  var filas = $('tbody tr');
  var filaselect = filas[index]
  var contractSelected = filaselect.cells[0];
  if(contractSelected.id == undefined || contractSelected.id == ""){
    contractSelected.id = 'contract_selected';
  }
  else{
    contractSelected.removeAttribute('id');
  }
}
//#####################################################################################//
//#                 Funciones de WS                                                   #//
//#####################################################################################//


// Para que funcione el WS refrescar el contacto desde dynamics a Inconcert
function mergeContacts(contact, contact_crm, contactCustomData) {
    console.log("mergeContacts");
   //console.log("Contact: ", JSON.stringify(contact));
    //console.log("contact_crm: ", JSON.stringify(contact_crm));
    var customData = {};
    try {
      // Parseo de datos personalizados del contacto
      customData = JSON.parse(contact.customData);
    } catch (err) {
      // Ocurrio un error al hacer el parse
      console.error("Ocurrio un error al obtener datos personalizados del contacto.");
      console.error(err);
    }
    if (contact_crm != undefined && contact_crm != {} && contact_crm.name != undefined && contact_crm.name == contact.id) {
      contact.email = contact_crm.eve_email || contact.email;
      contact.firstname = contact_crm.eve_nombre || contact.firstname;
      contact.lastname = contact_crm.eve_apellidos || contact.lastname;
      contact.phone = contact_crm.eve_telefono1 || contact.phone;
      contact.mobile = contact_crm.eve_telefono2 || contact.mobile;
      contact.fax = contact_crm.eve_telefono3 || contact.fax;
      contact.address1 = contact_crm.eve_tipovia || contact.address1;
      contact.address2 = contact_crm.eve_calle || contact.address2;
      contact.city = contact_crm.eve_poblacion || contact.city;
  
  
      customData.eve_portal = contact_crm.eve_portal || contactCustomData.eve_portal;
      customData.eve_calificativo = contact_crm.eve_calificativo || contactCustomData.eve_calificativo;
      customData.eve_descripcioncalificativo = contact_crm.eve_descripcioncalificativo || contactCustomData.eve_descripcioncalificativo;
      customData.eve_estado_anterior_deuda = contact_crm.eve_estado_anterior_deuda || contactCustomData.eve_estado_anterior_deuda;
      customData.eve_observacion_carta = contact_crm.eve_observacion_carta || contactCustomData.eve_observacion_carta;
      customData.eve_nif_nie = contact_crm.eve_nif_nie || contactCustomData.eve_nif_nie;
      customData.eve_numero = contact_crm.eve_numero || contactCustomData.eve_numero;
      customData.eve_escalera = contact_crm.eve_escalera || contactCustomData.eve_escalera;
      customData.eve_piso = contact_crm.eve_piso || contactCustomData.eve_piso;
      customData.eve_puerta = contact_crm.eve_puerta || contactCustomData.eve_puerta;
      customData.eve_codigoPostal = contact_crm.eve_codigoPostal || contactCustomData.eve_codigoPostal;
      customData.eve_consentimiento_grupo = contact_crm.eve_consentimiento_grupo || contactCustomData.eve_consentimiento_grupo;
      customData.eve_consentimiento_terceras = contact_crm.eve_consentimiento_terceras || contactCustomData.eve_consentimiento_terceras;
      customData.eve_origen_consentimiento = contact_crm.eve_origen_consentimiento || contactCustomData.eve_origen_consentimiento;
      customData.eve_fecha_consentimiento = contact_crm.eve_fecha_consentimiento || contactCustomData.eve_fecha_consentimiento;
      customData.eve_descripcionsociedad = contact_crm.eve_descripcionsociedad || contactCustomData.eve_descripcionsociedad;
      customData.eve_nombreapellidos = contact_crm.eve_nombreapellidos || contactCustomData.eve_nombreapellidos;
      customData.eve_envio_carta_deuda = contact_crm.eve_envio_carta_deuda || contactCustomData.eve_envio_carta_deuda;
      customData.eve_estado_deuda = contact_crm.eve_estado_deuda || contactCustomData.eve_estado_deuda;
      customData.eve_fecha_cita = contact_crm.eve_fecha_cita || contactCustomData.eve_fecha_cita;
      customData.eve_fecha_cita_adicional = contact_crm.eve_fecha_cita_adicional || contactCustomData.eve_fecha_cita_adicional;
      customData.eve_fecha_promesa_pago = contact_crm.eve_fecha_promesa_pago || contactCustomData.eve_fecha_promesa_pago;
      customData.eve_fecha_ultima_cita = contact_crm.eve_fecha_ultima_cita || contactCustomData.eve_fecha_ultima_cita;
      customData.eve_fecha_ultima_gestion = contact_crm.eve_fecha_ultima_gestion || contactCustomData.eve_fecha_ultima_gestion;
      customData.eve_multiplessociedades = contact_crm.eve_multiplessociedades || contactCustomData.eve_multiplessociedades;
      customData.eve_gestion_de_impagos = contact_crm.eve_gestion_de_impagos || contactCustomData.eve_gestion_de_impagos;
      customData.eve_gestion_de_impagos_adicional = contact_crm.eve_gestion_de_impagos_adicional || contactCustomData.eve_gestion_de_impagos_adicional;
      customData.eve_gestora_deuda = contact_crm.eve_gestora_deuda || contactCustomData.eve_gestora_deuda;
      customData.eve_deudatotalinquilino = contact_crm.eve_deudatotalinquilino || contactCustomData.eve_deudatotalinquilino;
      customData.eve_deudatotalcontrato = contact_crm.eve_deudatotalcontrato || contactCustomData.eve_deudatotalcontrato;
      customData.eve_observacion_gestion = contact_crm.eve_observacion_gestion || contactCustomData.eve_observacion_gestion;
      customData.eve_observaciones_deuda = contact_crm.eve_observaciones_deuda || contactCustomData.eve_observaciones_deuda;
      customData.eve_deudatotalplandepago = contact_crm.eve_deudatotalplandepago || contactCustomData.eve_deudatotalplandepago;
      customData.eve_deudatotalanticipo = contact_crm.eve_deudatotalanticipo || contactCustomData.eve_deudatotalanticipo;
      customData.eve_importe_total_pendiente_vencimiento_plan = contact_crm.eve_importe_total_pendiente_vencimiento_plan || contactCustomData.eve_importe_total_pendiente_vencimiento_plan;
      customData.codigo_campanya_comercial = contact_crm.codigo_campanya_comercial || contactCustomData.codigo_campanya_comercial
  
      contact.customData = JSON.stringify(customData);
  
      contact.eve_nif_nie = contact_crm.eve_nif_nie || contact.eve_nif_nie;
      contact.eve_numero = contact_crm.eve_numero || contact.eve_numero;
      contact.eve_escalera = contact_crm.eve_escalera || contact.eve_escalera;
      contact.eve_piso = contact_crm.eve_piso || contact.eve_piso;
      contact.eve_puerta = contact_crm.eve_puerta || contact.eve_puerta;
      contact.eve_codigoPostal = contact_crm.eve_codigoPostal || contact.eve_codigoPostal;
      contact.eve_fecha_consentimiento = contact_crm.eve_fecha_consentimiento || contact.eve_fecha_consentimiento;
      contact.eve_descripcionsociedad = contact_crm.eve_descripcionsociedad || contact.eve_descripcionsociedad;
      contact.name = contact_crm.name || contact.name;
      contact.eve_nombreapellidos = contact_crm.eve_nombreapellidos || contact.eve_nombreapellidos;
      contact.eve_fecha_cita = contact_crm.eve_fecha_cita || contact.eve_fecha_cita;
      contact.eve_fecha_cita_adicional = contact_crm.eve_fecha_cita_adicional || contact.eve_fecha_cita_adicional;
      contact.eve_fecha_ultima_gestion = contact_crm.eve_fecha_ultima_gestion || contact.eve_fecha_ultima_gestion;
      contact.eve_gestion_de_impagos = contact_crm.eve_gestion_de_impagos || contact.eve_gestion_de_impagos;
      contact.eve_gestion_de_impagos_adicional = contact_crm.eve_gestion_de_impagos_adicional || contact.eve_gestion_de_impagos_adicional;
      contact.eve_gestora_deuda = contact_crm.eve_gestora_deuda || contact.eve_gestora_deuda;
      contact.eve_observacion_gestion = contact_crm.eve_observacion_gestion || contact.eve_observacion_gestion;
      contact.eve_observaciones_deuda = contact_crm.eve_observaciones_deuda || contact.eve_observaciones_deuda;
      contact.eve_fecha_promesa_pago = contact_crm.eve_fecha_promesa_pago || contact.eve_fecha_promesa_pago;
      contact.eve_deudatotalinquilino = contact_crm.eve_deudatotalinquilino || contact.eve_deudatotalinquilino;
      contact.eve_deudatotalcontrato = contact_crm.eve_deudatotalcontrato || contact.eve_deudatotalcontrato;
      contact.eve_deudatotalplandepago = contact_crm.eve_deudatotalplandepago || contact.eve_deudatotalplandepago;
      contact.eve_deudatotalanticipo = contact_crm.eve_deudatotalanticipo || contact.eve_deudatotalanticipo;
  
    }
    //console.log("resultado: ", JSON.stringify(contact));
    return contact;
  }

// Funcion para crear Campaña Comercial en Dynamics
async function crearCampanyaComercial(_wsUrl, _contact){

  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "text/plain");
  console.log("contact.eve_contratoid: " + _contact.eve_contratoid);
  
  if(_contact.eve_contratoid == undefined || _contact.eve_contratoid == "" || _contact.eve_contratoid.includes("FI")){
    notificationsService.error("Alerta", "El contacto no tiene asociado un contrato válido. Debe elegir uno.");
    // Cargo datos de la tabla
    component.LoadTable();
    $("#tablaContratos").show();
    

  }
  else{
    var customDataAux = {};
      try {
        // Parseo de datos personalizados del contacto
        customData = JSON.parse(_contact.customData);
      } catch (err) {
        // Ocurrio un error al hacer el parse
        console.error("Ocurrio un error al obtener datos personalizados del contacto.");
        console.error(err);
      }
    var raw = JSON.stringify({
      "eve_codigo_contrato_id@odata.bind": "/eve_contratos(" +_contact.eve_contratoid + ")",
      "eve_inquilino_principal_id": customDataAux.externalid || undefined,
      "eve_tipo_recuperacion": _contact.eve_tipo_recuperacion || undefined,
      "eve_motivo_inquilino": _contact.eve_motivo_inquilino || undefined,
      "eve_otros_motivos_i": _contact.eve_otros_motivos_i || undefined,
      "eve_otros_motivos_ii": _contact.eve_otros_motivos_ii || undefined,
      "eve_poblacion_destino": _contact.eve_poblacion_destino || undefined,
      "eve_codigo_postal_destino": _contact.eve_codigo_postal_destino || undefined,
      "eve_comentarios_motivo_finalizacion": _contact.eve_comentarios_motivo_finalizacion || undefined,
      "eve_cierre_expediente": _contact.eve_cierre_expediente || undefined,
      "eve_inquilino_recuperacion_1": _contact.eve_inquilino_recuperacion_1 || undefined,
      "eve_email_recuperacion_inquilino_1": _contact.eve_email_recuperacion_inquilino_1 || undefined,
      "eve_telefono_recuperacion_1": _contact.eve_telefono_recuperacion_1 || undefined
    });
    
    console.log("Datos para crear la campaña comercial: " + raw);
    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
    mode: 'cors'
    };
        
    try{
      const response = await fetch(_wsUrl + "/api/CRM/campanya-comercial-nueva", requestOptions);
      const text = await response.text();
      console.log('Respuesta WS ', text);
        if (text.length != 36){
            notificationsService.error("Dynamics", "La campaña comercial no se creó en Dynamics");
        }
        else{
            notificationsService.success("Dynamics", "La campaña comercial se creó en Dynamics");
            return text;
        }
    }catch(err){
      console.error(err); 
    }
  }
}

function CrearBotonGuardarCC(){
  var html = component.document;
  var button = html.createElement('button');
  button.setAttribute("id", "btnGuardarCC");

  var div1 = html.createElement('div');
  var div2 = html.createElement('div');
  var div3 = html.createElement('div');
  
  button.innerHTML = 'Crear campaña comercial';
  button.classList.add("btn");
  button.classList.add("btn-secondary");
  button.classList.add("float-left");

  button.style.cssText = 'margin-top: 23px; display:inline-block, min-width: 26% !important;';

  div1.classList.add("col-12");
  div1.classList.add("col-md-4");
  div1.classList.add("col-lg-3");
  div1.classList.add("ng-star-inserted");

  div2.classList.add("input-group");
  div2.classList.add("input-group-sm");
  div2.classList.add("ng-star-inserted");

  div3.classList.add("form-group");
  div3.classList.add("ng-tns-c8-1");
  div3.classList.add("ng-star-inserted");

  var ccgroup = html.querySelectorAll('[group="Propiedades Campaña Comercial"]')[0];;
  var card_block = ccgroup.getElementsByClassName('row')[0];
  
  card_block.appendChild(div1);
  div1.appendChild(div2);
  div2.appendChild(div3);
  div3.appendChild(button);

  button.onclick = function(){
    console.log('Crear Campaña Comercial');
    try {
      var resultado = crearCampanyaComercial(wsUrl, contact);
      
      resultado.then(text => {
        if(text){
          obtenerCodigo_campanya_comercial(wsUrl,contact,text);
        }
      });

    } catch (error) {
      if(error != undefined){
        console.error(error);
      }else{
        console.info("Problema al crear la campaña comercial");
      }
      
    }
    
  };
}

async function obtenerCodigo_campanya_comercial(_wsUrl, _contact, campanya_comercial_id){
  var requestOptions = {
      method: 'GET',
      redirect: 'follow',
      mode: 'cors'
    };
    try{
      const response = await fetch(_wsUrl + "/api/CRM/GetCampanyaComercial?id=" + campanya_comercial_id, requestOptions);
      const json = await response.json();
      console.log('Respuesta WS ', json);
      if(json.eve_name){
        var codigo_campanya_comercial = json.eve_name;
        console.log("codigo_campanya_comercial: " + codigo_campanya_comercial)
        var customDataAux = {}
        try {
            customDataAux = JSON.parse(_contact.customData);
            customDataAux["codigo_campanya_comercial"] = codigo_campanya_comercial;
            _contact.customData = JSON.stringify(customDataAux);

        } catch (err) {
          console.error("Ocurrio un error al obtener datos personalizados del contacto.");
          console.error(err);
        }
        contact = _contact;
        console.log(contact);
        $("#btnGuardarCC").hide();
        $("#customProperties_codigo_campanya_comercial").show();
        $("#tablaContratos").hide();
        component.InitializeContact();

      }
    }catch(err){
      console.log(err); 
    }
       
}
//#####################################################################################//
//#                 Funcion formato de fecha                                          #//
//#####################################################################################//

function formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear(),
      hour = d.getHours(),
      minutes = d.getMinutes(),
      seconds = d.getSeconds();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;
  if (hour.length < 2) hour = '0' + hour;
  if (minutes.length < 2) minutes = '0' + minutes;
  if (seconds.length < 2) seconds = '0' + seconds;
  var respuesta = [year, month, day].join('-') + " " + [hour,minutes,seconds].join(':');
  return respuesta;
}