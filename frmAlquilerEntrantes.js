// Referencias a elementos del formulario
var contact;
var interaction;
var contactInputsById;
var notificationsService;

// Datos custom del contacto
var contactCustomData = {};

// Productos del contacto
var contactProducts = [];

//Necesarios para el WS
var actualizar = false;
var crearContactoLead = true;
var num_onLoaded = 0;
var wsUrl = "https://5d605.inconcertcc.com:10443/";
var component = undefined;

// Datos asociados a la tabla
this.tableLoading = false;
this.tableData = [];
this.tableHtml = '';
this.selectedData = {};
this.selectedHtml = '';

this.tableHtmlDetail = '';

var promocionlocalidad = '';
var promocionzona = '';
var promociontipoinmueble = '';
var promociontipologia = '';
var promociondormitorios = '';

// Evento de formulario inicializado
this.onReady = function() {

    // Obtener y guardar referencias a elementos del formulario
    contact = this.contact;
    interaction = this.interaction;
    contactInputsById = this.contactInputsById;
    notificationsService = this.notificationsService;

    // Si hay interacción programar un timer de 1 minuto
    if (interaction) setTimeout(function() {
        // Mostrar notificación de nivel info al usuario
        notificationsService.info("Información", "La interacción lleva 1 minuto.");
    }, 60000);

    // Si hay interacción programar un timer de 5 minutos
    if (interaction) setTimeout(function() {
        // Mostrar notificación de nivel info al usuario
        notificationsService.info("Información", "La interacción lleva 5 minutos.");
    }, 300000);
    
};

// Evento de contacto cargado
this.onLoaded = function(_contact) {
    num_onLoaded++;
    console.log("num_onLoaded " + num_onLoaded)
    // Hay que actualizar la referencia al objeto contacto
    contact = _contact;

    

    // Escribir a log
    if (_contact) console.log("Se cargaron datos del contacto '" + _contact.id + "'");
    if (_contact.id == undefined){
        crearcontacto = true;
        console.log("Crear contacto '" + crearcontacto + "'");
    }else {
        try {
            // Parseo de datos personalizados del contacto
            contactCustomData = JSON.parse(contact.customData);    
        } catch (err) {
            // Ocurrio un error al hacer el parse
            console.error("Ocurrio un error al obtener datos personalizados del contacto.");
            console.error(err);
        }
        // Control de cuando se crea el contacto-lead en Dynamics
        if (num_onLoaded > 1 && _contact.id != undefined && crearContactoLead){
            console.log("crear contacto-lead " + crearContactoLead)
            component = this;
            crearContacto_lead(wsUrl,_contact);
            crearContactoLead = false;
        }
        
    }

    // Obtengo productos del contacto
    this.contactService.GetContactProducts(_contact.id).subscribe(
        result => {
            // Guardo el dato obtenido
            contactProducts = result || contactProducts;
            //console.log(contactProducts)
        },
        err => {
            // Mostrar notificación de nivel info al usuario
            notificationsService.info("Error", "No se pudo obtener productos del contacto.");
            // Escribo a consola
            console.error("No se pudo obtener productos del contacto.");
            console.error(err);
        }
    );
    
    $("#customProperties_eve_codigopromocion").hide();
    $("#customProperties_eve_promocionid").hide();
    $("#customProperties_eve_descripcionpromocion").hide();
    $("#customProperties_eve_api").hide();
    
    //$('.card-block').html("hola");
    
    var campaing = _contact.createdByCampaignId;
    console.log('campaña: ' + campaing);

    if(campaing == null) campaing = 'Alquiler_Entrantes_Testa';
    console.log('campaña: ' + campaing);
    if(campaing == 'Alquiler_Entrantes_Testa' || campaing == 'Alquiler_Entrantes_Fidere'){
        var origen = '100000000'
        
        var property = this.contactProperties.find(item => item.id === 'eve_origen');
        if (property) {
            property.propertyValue = origen;
            this.OnContactPropertyValueChanged('eve_origen', origen);
            this.setContactProperty('eve_origen', origen);
        }
    }
    
    if(campaing == 'Alquiler_Email_Testa' || campaing == 'Alquiler_Email_Fidere'){
        var origen = '100000002'
        
        var property = this.contactProperties.find(item => item.id === 'eve_origen');
        if (property) {
            property.propertyValue = origen;
            this.OnContactPropertyValueChanged('eve_origen', origen);
            this.setContactProperty('eve_origen', origen);
        }
    }
    
} 

// Evento de cambio en propiedad de contacto
this.onContactPropertyChanged = function(property, value, _contact) {
    // Para actualizar el contacto en su CRM
    actualizar = true;
    console.log("La información del contacto se actualizará al guardar: " + actualizar);

    
    // Escribir a log
    console.log("Información", "La propiedad '" + property + "' cambió a valor '" + value + "'");
    
    if(property == "eve_dormitorios"){
        
        promociondormitorios = this.contactPropertiesById[property].propertyValue ? this.contactPropertiesById[property].propertyValue.GetDescription() : null;
        
        // Cargo datos de la tabla
        this.LoadTable(promocionlocalidad, promocionzona, promociontipoinmueble, promociontipologia, promociondormitorios);
    }
    
    if(property == "eve_tipo_inmueble"){
        
        promociontipoinmueble = this.contactPropertiesById[property].propertyValue ? this.contactPropertiesById[property].propertyValue.GetDescription() : null;
        
        // Cargo datos de la tabla
        this.LoadTable(promocionlocalidad, promocionzona, promociontipoinmueble, promociontipologia, promociondormitorios);
    }
    
    if(property == "eve_poblacion"){
        
        promocionlocalidad = this.contactPropertiesById[property].propertyValue ? this.contactPropertiesById[property].propertyValue.GetDescription() : null;
        
        // Cargo datos de la tabla
        this.LoadTable(promocionlocalidad, promocionzona, promociontipoinmueble, promociontipologia, promociondormitorios);
    }
    
    if(property == "eve_zona"){
        
        promocionzona = this.contactPropertiesById[property].propertyValue ? this.contactPropertiesById[property].propertyValue.GetDescription() : null;
        
        // Cargo datos de la tabla
        this.LoadTable(promocionlocalidad, promocionzona, promociontipoinmueble, promociontipologia, promociondormitorios);
    }
    
    if(property == "eve_tipologia_viviendas"){
        
        promociontipologia = this.contactPropertiesById[property].propertyValue ? this.contactPropertiesById[property].propertyValue.GetDescription() : null;
     
        // Cargo datos de la tabla
        this.LoadTable(promocionlocalidad, promocionzona, promociontipoinmueble, promociontipologia, promociondormitorios);
    }
    
};

// Evento de antes de guardar
this.onBeforeSave = function(_contact, mustFinishInteraction) {

    // Flag de impedir guardado
    var preventSave = false;

    // Si el formulario muestra el firstname y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.firstname && _contact && _contact.firstname && !validateAlphanumeric(_contact.firstname)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El nombre ingresado para el contacto (" + _contact.firstname + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el lastname y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.lastname && _contact && _contact.lastname && !validateAlphanumeric(_contact.lastname)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El apellido ingresado para el contacto (" + _contact.lastname + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el email y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.email && _contact && _contact.email && !validateEmail(_contact.email)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El correo ingresado para el contacto (" + _contact.email + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }

    // Si el formulario muestra el phone y hay un dato ingresado hay que validar que sea correcto
    if (contactInputsById.phone && _contact && _contact.phone && !validatePhone(_contact.phone)) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "El teléfono ingresado para el contacto (" + _contact.phone + ") no es válido");
        // Interrumpir el guardado
        preventSave = true;
    }
    // Para actualizar el contacto en su CRM
    // Permitir el guardado
        //if (actualizar) {
          //  actualizarContacto(wsUrl, contact, contactCustomData);
        //}
        
   /* //Asociar producto a un inquilino
    this.executeProcedure(
        "TESTA_AsociarProducto",
        [
            { column: "ContactId", value: _contact.id },
            { column: "ProductId", value: _contact.ID_Inmueble },
        ],
        function(error, recordset) {
            // Verifico qsi hubo error
            if (error) {
                // Muestro notificación de error
                notificationsService.error("Error", "Ocurrió un error, no se pudo asociar la promoción.");
                console.log(error);
            }
            notificationsService.success("Promoción asociada al inquilino correctamente");
        }
    );*/
    
    //Envio de correo de llamada especial
    if(_contact.llamada_especial == "true"){
        
        if(_contact.eve_nif_nie == undefined){
            var nie = "0";
        }else{
            var nie = _contact.eve_nif_nie;
        }
        
        this.executeProcedure(
        "TESTA_EnvioM_LlamadaEspecial",
        [
            { column: "Nombre", value: _contact.firstname + ' ' + _contact.lastname },
            { column: "Telefono", value: _contact.phone },
            { column: "Email", value: _contact.email },
            { column: "Nie", value: nie },
            { column: "OrigenLLamada", value: _contact.eve_origen },
            { column: "Observaciones", value: _contact.eve_observaciones },
    
        ],
        function(error, recordset) {

            // Verifico qsi hubo error
            if (error) {
                // Muestro notificación de error
                notificationsService.error("Error", "Ocurrió un error, no se pudo enviar el correo.");
                console.log(error);
            }
            notificationsService.success("Se ha enviado el mail de llamada especial correctamente");
            console.log("Se ha enviado el mail de llamada especial correctamente")
        }
        );
    }
    
    return !preventSave;
}

// Evento de despues de guardar
this.onAfterSave = function(_contact) {

    // Escribir a log
    if (_contact) console.log("Se guardaron datos del contacto '" + _contact.id + "'");

}
// Evento de despues de crear
this.onAfterCreate = function(_contact){
    if (_contact) console.log("Se creo el contacto '" + _contact.id + "'");
}


// Función de validación de campos de correo
function validateEmail(value) {

    // Expresión regular para validar campos de correo
    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!EMAIL_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

// Función de validación de campos de teléfono
function validatePhone(value) {

    // Expresión regular para validar campos de teléfono
    const PHONE_REGEXP = /^[0-9\-\+\(\)\s]+$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!PHONE_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

// Función de validación de campos alfanuméricos
function validateAlphanumeric(value) {

    // Expresión regular para validar campos alfanuméricos
    const ALPHA_REGEXP = /^[a-zA-Z\-\_ñÑàèìòùáéíóúäëïöüÀÈÌÒÙÁÉÍÓÚÄËÏÖÜ\.0-9\-\+\(\)\s]+$/;

    // Si no se cumple la expresión, la validación no es correcta
    if (!ALPHA_REGEXP.test(value)) return false;

    // La validación es correcta
    return true;
}

///////////////////////////////////////
// Funciones para manejo de la tabla //
///////////////////////////////////////

// Función para hacer la consulta de datos y actualizar la tabla
this.LoadTable = function(poblacion, zona, tipoinmueble, tipologia, dormitorios) {
    
    // Verifico que no exista una consulta en proceso
    if (this.tableLoading) {
        // Mostrar notificación de nivel info al usuario
        notificationsService.warn("Atención", "Existe una consulta de datos en proceso.");
        // Devuelvo false
        return false;
    }

    // Limpio el detalle mostrado
    this.SetTableResultDetail(-1);

    // Paso la tabla a modo loading
    this.SetTableLoading();

    // Ejecuto consulta de inmuebles
    this.executeProcedure(
        "TESTA_GetPromociones",
        [
            { column: "Poblacion", value: poblacion },
            { column: "Zona", value: zona },
            { column: "TipoInmueble", value: tipoinmueble },
            { column: "Tipologia", value: tipologia },
            { column: "Dormitorios", value: dormitorios },
    
        ],
        function(error, recordset) {

            // Verifico qsi hubo error
            if (error) {
                // Muestro notificación de error
                notificationsService.error("Error", "Ocurrió un error al consultar inmuebles del contacto.");
            }
            
            // Cargo resultados obtenidos en la tabla
            this.SetTableResults(recordset || []);

        }.bind(this)
    );
    
    // Devuelvo false
    return false;
}

// Función para cambiar la tabla a modo loading
this.SetTableLoading = function() {
    
    // Actualizo flag de tabla cargando
    this.tableLoading = true;

    // Cargo HTML de tabla en modo "cargando"
    this.BuildTable(`
        <tr>
            <td colspan="11">
                <div>
                    <i class="fas fa-spinner fa-spin loading-spinner"></i>
                </div>
            </td>
        </tr>
    `);

    // Refresco el HTML custom
    this.RefreshCustomHtml();
}

// Función para cambiar la tabla a modo sin resultados
this.SetTableNoResults = function() {
    
    // Actualizo flag de tabla cargando
    this.tableLoading = false;

    // Actualizo la variable de datos
    this.tableData = [];

    // Cargo HTML de tabla en modo "sin resultados"
    this.BuildTable(`
        <tr class="noRecordsRow">
            <td colspan="11">
                <div class="alert alert-info mt-0" role="alert">
                    <span class="fas fa-info"></span>
                    <strong> No se encontraron resultados </strong> 'Aún no hay elementos creados, o que coincidan con la búsqueda actual.'
                </div>
            </td>
        </tr>
    `);

    // Refresco el HTML custom
    this.RefreshCustomHtml();
}

// Función para cargar los resultados a la tabla
this.SetTableResults = function(data) {

    // Verifico si se obtuvieron resultados
    if (!data || !data.length) {
        // Paso la tabla a modo sin resultados
        this.SetTableNoResults();
        // No sigo
        return;
    }
    
    // Actualizo flag de tabla cargando
    this.tableLoading = false;

    // Actualizo la variable de datos
    this.tableData = data;

    // HTML de las filas
    var rowsHtml = '';

    // Recorro el array de datos y los agrego como filas del HTML
    data.forEach(
        (row, index) => rowsHtml = rowsHtml + `
            <tr class="cursor-pointer" onclick="this.SetTableResultDetail(${index})">
                <td>${row.Premium || ''}</td>
                <td>${row.Direccion || ''}</td>
                <td>${row.Estado || ''}</td>
                <td>${row.TipoVivienda || ''}</td>
                <td>${row.UnDormitorio || ''}</td>
                <td>${row.DosDormitorios || ''}</td>
                <td>${row.TresDormitorios || ''}</td>
                <td>${row.CuatroDormitorios || ''}</td>
                <td>${row.Locales || ''}</td>
                <td>${row.Garaje || ''}</td>
                <td>${row.Trastero || ''}</td>
            </tr>`
    );

    // Cargo HTML de tabla en modo "con resultados"
    this.BuildTable(rowsHtml);

    // Refresco el HTML custom
    this.RefreshCustomHtml();
}

// Función para construir el HTML de la tabla
this.BuildTable = function(tableContent) {

    // Cargo HTML de tabla en modo "con resultados"
    this.tableHtml = `
<table class="table table-sm table-striped table-hover table-bordered editableTable">
    <thead class="thead-inverse">
        <tr>
            <th style="width: 5%">Premium</th>
            <th style="width: 10%">Direccion</th>
            <th style="width: 5%">Estado</th>
            <th style="width: 10%">Tipo</th>
            <th style="width: 10%">1 Dormitorio</th>
            <th style="width: 10%">2 Dormitorios</th>
            <th style="width: 10%">3 Dormitorios</th>
            <th style="width: 10%">4 Dormitorios</th>
            <th style="width: 10%">Locales</th>
            <th style="width: 10%">Garaje</th>
            <th style="width: 10%">Trasteros</th>
        </tr>
    </thead>
    <tbody>
        ${tableContent || ''}
    </tbody>
</table>
    `;
}

// Función para seleccionar un elemento y guardar el detalle de la promoción
this.SetTableResultDetail = function(index) {

    // Obtengo el dato seleccionado
    var row = this.tableData ? this.tableData[index] : null;
    
    // Verifico que se haya obtenido un dato
    if (!row) {
        // Limpio as variables
        this.selectedData = null;
        this.selectedHtml = '';
        
        // No sigo
        return;
    }

    // Guardo el elemento seleccionado
    this.selectedData = row;
    
    var codigopromocion = this.selectedData.CodigoPromocion;
    var codigopromocionid = this.selectedData.IdPromocion;
    var descripcionpromocion = this.selectedData.DescripcionPromocion;
    var direccionpromocion = this.selectedData.Direccion;
    var responsablecomercial = this.selectedData.Responsable;
    var idusuario = this.selectedData.IdUsuario;
    var comercializadora = this.selectedData.Comercializadora;
    var api = this.selectedData.Api;
    var sociedad = this.selectedData.Sociedad;
    
    var property = this.contactProperties.find(item => item.id === 'eve_codigopromocion');
    if (property) {
        property.propertyValue = codigopromocion;
        this.OnContactPropertyValueChanged('eve_codigopromocion', codigopromocion);
    }
    
    var property = this.contactProperties.find(item => item.id === 'eve_promocionid');
    if (property) {
        property.propertyValue = codigopromocionid;
        this.OnContactPropertyValueChanged('eve_promocionid', codigopromocionid);
    }
    
    var property = this.contactProperties.find(item => item.id === 'eve_descripcionpromocion');
    if (property) {
        property.propertyValue = descripcionpromocion;
        this.OnContactPropertyValueChanged('eve_descripcionpromocion', descripcionpromocion);
    }
    
    var property = this.contactProperties.find(item => item.id === 'eve_direccion');
    if (property) {
        property.propertyValue = direccionpromocion;
        this.OnContactPropertyValueChanged('eve_direccion', direccionpromocion);
    }
    
    var property = this.contactProperties.find(item => item.id === 'eve_nombre_comercializadora');
    if (property) {
        property.propertyValue = comercializadora;
        this.OnContactPropertyValueChanged('eve_nombre_comercializadora', comercializadora);
    }
    
    var property = this.contactProperties.find(item => item.id === 'eve_api');
    if (property) {
        property.propertyValue = api;
        this.OnContactPropertyValueChanged('eve_api', api);
    }
    
    var property = this.contactProperties.find(item => item.id === 'eve_sociedad_origen');
    if (property) {
        property.propertyValue = sociedad;
        this.OnContactPropertyValueChanged('eve_sociedad_origen', sociedad);
    }
    
    var property = this.contactProperties.find(item => item.id === 'owner');
    if (property) {
        property.propertyValue = idusuario;
        this.OnContactPropertyValueChanged('owner', idusuario);
        this.setContactProperty('owner', idusuario);
    }
}

///////////////////////////////////////
// Funciones para el WS //
///////////////////////////////////////

function crearContacto_lead(_wsUrl, _contact){
    console.log("crearContacto_lead");
    var _externalid = "";
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");

    if(_contact.eve_empadronado == 1){
        _contact.eve_empadronado = true;
    }else{
        _contact.eve_empadronado = false;
    }

    if(_contact.eve_vivienda_propiedad == 1){
        _contact.eve_vivienda_propiedad = true;
    }else{
        _contact.eve_vivienda_propiedad = false;
    }

    if(_contact.eve_info_comercial_empresa_colaboradora == 1){
        _contact.eve_info_comercial_empresa_colaboradora = true;
    }else{
        _contact.eve_info_comercial_empresa_colaboradora = false;
    }

    if(_contact.eve_info_comercial_empresa_grupo == 1){
        _contact.eve_info_comercial_empresa_grupo = true;
    }else{
        _contact.eve_info_comercial_empresa_grupo = false;
    }

    if(_contact.eve_info_testa_finalizado_contrato == 1){
        _contact.eve_info_testa_finalizado_contrato = true;
    }else{
        _contact.eve_info_testa_finalizado_contrato = false;
    }

    if(_contact.eve_politica_proteccion_datos == 1){
        _contact.eve_politica_proteccion_datos = true;
    }else{
        _contact.eve_politica_proteccion_datos = false;
    }

    var raw = JSON.stringify({
        "eve_name": _contact.id,
        "eve_origen": _contact.eve_origen || undefined,
        "eve_tipo_inmueble": _contact.eve_tipo_inmueble || undefined,
        "eve_clasificacion": _contact.eve_clasificacion || undefined,
        "eve_nombre": _contact.firstname || undefined,
        "eve_apellidos": _contact.lastname || undefined,
        "eve_documento_identidad": _contact.eve_nif_nie || undefined,
        "eve_telefono": _contact.phone || undefined,
        "eve_email": _contact.email || undefined,
        "eve_codigo_postal": _contact.eve_codigopostal || undefined,
        "eve_profesion": _contact.eve_profesion || undefined,
        "eve_estado_civil": _contact.eve_estado_civil || undefined,
        "eve_empadronado": _contact.eve_empadronado || undefined,
        "eve_vivienda_propiedad": _contact.eve_vivienda_propiedad || undefined,
        "eve_ingresos_mensuales": parseFloat(_contact.eve_ingresos_mensuales || 0),
        "eve_observaciones": _contact.eve_observaciones || undefined,
        "eve_campanya": _contact.eve_campanya || undefined,
        "eve_como_conocido": _contact.eve_como_conocido || undefined,
        "eve_zona_localidad": _contact.eve_poblacion || undefined,
        "eve_promocion_id": _contact.eve_promocionid || undefined,
        "eve_politica_proteccion_datos": _contact.eve_politica_proteccion_datos || undefined,
        "eve_info_comercial_empresa_colaboradora": _contact.eve_info_comercial_empresa_colaboradora || undefined,
        "eve_info_comercial_empresa_grupo": _contact.eve_info_comercial_empresa_grupo || undefined,
        "eve_info_testa_finalizado_contrato": _contact.eve_info_testa_finalizado_contrato || undefined       
      });

    console.log('Datos enviados a Dynamics: ', raw)
    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
    mode: 'cors'
    };

    fetch(_wsUrl + "/api/CRM/contacto-lead-nuevo", requestOptions)
    .then(function(response) {
        return response.text();
      })
      .then(function(text) {
        console.log('Respuesta WS ', text);
        if (text.length != 36){
            notificationsService.error("Dynamics", "El contacto NO se creó en Dynamics");
        }
        else{
            //notificacionsService.success("Dynamics", "El contacto-lead se creo en Dynamics");
        }
      })
      .catch(function(error) {
        console.log('Respuesta WS', error)
        notificationsService.error("Dynamics", "El contacto NO se creó en Dynamics");
      });
}
  
